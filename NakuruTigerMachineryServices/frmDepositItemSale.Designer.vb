﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDepositItemSale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SizeLabel As System.Windows.Forms.Label
        Dim UnitPriceLabel As System.Windows.Forms.Label
        Dim ItemsSoldLabel1 As System.Windows.Forms.Label
        Dim TotalPriceLabel1 As System.Windows.Forms.Label
        Dim AmountDepositedLabel As System.Windows.Forms.Label
        Dim DateDepositedLabel As System.Windows.Forms.Label
        Dim AmountDueLabel As System.Windows.Forms.Label
        Dim DueDateLabel As System.Windows.Forms.Label
        Dim FineLabel As System.Windows.Forms.Label
        Dim CustIDLabel As System.Windows.Forms.Label
        Dim CustNameLabel As System.Windows.Forms.Label
        Dim IDLabel As System.Windows.Forms.Label
        Dim NameLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim QuantityLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDepositItemSale))
        Me.SizeTextBox = New System.Windows.Forms.TextBox()
        Me.UnitPriceTextBox = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.DueDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.FineTextBox = New System.Windows.Forms.TextBox()
        Me.ItemsSoldTextBox = New System.Windows.Forms.TextBox()
        Me.TotalPriceTextBox = New System.Windows.Forms.TextBox()
        Me.AmountDueTextBox = New System.Windows.Forms.TextBox()
        Me.AmountGivenTextBox = New System.Windows.Forms.TextBox()
        Me.DateDepositedDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.QuantityItemTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        Me.CustNameTextBox = New System.Windows.Forms.TextBox()
        Me.NameTextBox = New System.Windows.Forms.TextBox()
        Me.ItemIDComboBox = New System.Windows.Forms.ComboBox()
        Me.NewItemButton = New System.Windows.Forms.Button()
        Me.btnAddToCart = New System.Windows.Forms.Button()
        Me.CusIDComboBox = New System.Windows.Forms.ComboBox()
        Me.PrintForm1 = New Microsoft.VisualBasic.PowerPacks.Printing.PrintForm(Me.components)
        SizeLabel = New System.Windows.Forms.Label()
        UnitPriceLabel = New System.Windows.Forms.Label()
        ItemsSoldLabel1 = New System.Windows.Forms.Label()
        TotalPriceLabel1 = New System.Windows.Forms.Label()
        AmountDepositedLabel = New System.Windows.Forms.Label()
        DateDepositedLabel = New System.Windows.Forms.Label()
        AmountDueLabel = New System.Windows.Forms.Label()
        DueDateLabel = New System.Windows.Forms.Label()
        FineLabel = New System.Windows.Forms.Label()
        CustIDLabel = New System.Windows.Forms.Label()
        CustNameLabel = New System.Windows.Forms.Label()
        IDLabel = New System.Windows.Forms.Label()
        NameLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        QuantityLabel = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SizeLabel
        '
        SizeLabel.AutoSize = True
        SizeLabel.Location = New System.Drawing.Point(24, 68)
        SizeLabel.Name = "SizeLabel"
        SizeLabel.Size = New System.Drawing.Size(30, 13)
        SizeLabel.TabIndex = 4
        SizeLabel.Text = "Size:"
        '
        'UnitPriceLabel
        '
        UnitPriceLabel.AutoSize = True
        UnitPriceLabel.Location = New System.Drawing.Point(24, 146)
        UnitPriceLabel.Name = "UnitPriceLabel"
        UnitPriceLabel.Size = New System.Drawing.Size(56, 13)
        UnitPriceLabel.TabIndex = 8
        UnitPriceLabel.Text = "Unit Price:"
        '
        'ItemsSoldLabel1
        '
        ItemsSoldLabel1.AutoSize = True
        ItemsSoldLabel1.Location = New System.Drawing.Point(13, 10)
        ItemsSoldLabel1.Name = "ItemsSoldLabel1"
        ItemsSoldLabel1.Size = New System.Drawing.Size(59, 13)
        ItemsSoldLabel1.TabIndex = 12
        ItemsSoldLabel1.Text = "Items Sold:"
        '
        'TotalPriceLabel1
        '
        TotalPriceLabel1.AutoSize = True
        TotalPriceLabel1.Location = New System.Drawing.Point(13, 36)
        TotalPriceLabel1.Name = "TotalPriceLabel1"
        TotalPriceLabel1.Size = New System.Drawing.Size(61, 13)
        TotalPriceLabel1.TabIndex = 14
        TotalPriceLabel1.Text = "Total Price:"
        '
        'AmountDepositedLabel
        '
        AmountDepositedLabel.AutoSize = True
        AmountDepositedLabel.Location = New System.Drawing.Point(13, 62)
        AmountDepositedLabel.Name = "AmountDepositedLabel"
        AmountDepositedLabel.Size = New System.Drawing.Size(97, 13)
        AmountDepositedLabel.TabIndex = 16
        AmountDepositedLabel.Text = "Amount Deposited:"
        '
        'DateDepositedLabel
        '
        DateDepositedLabel.AutoSize = True
        DateDepositedLabel.Location = New System.Drawing.Point(13, 89)
        DateDepositedLabel.Name = "DateDepositedLabel"
        DateDepositedLabel.Size = New System.Drawing.Size(84, 13)
        DateDepositedLabel.TabIndex = 18
        DateDepositedLabel.Text = "Date Deposited:"
        '
        'AmountDueLabel
        '
        AmountDueLabel.AutoSize = True
        AmountDueLabel.Location = New System.Drawing.Point(13, 114)
        AmountDueLabel.Name = "AmountDueLabel"
        AmountDueLabel.Size = New System.Drawing.Size(69, 13)
        AmountDueLabel.TabIndex = 20
        AmountDueLabel.Text = "Amount Due:"
        '
        'DueDateLabel
        '
        DueDateLabel.AutoSize = True
        DueDateLabel.Location = New System.Drawing.Point(13, 140)
        DueDateLabel.Name = "DueDateLabel"
        DueDateLabel.Size = New System.Drawing.Size(56, 13)
        DueDateLabel.TabIndex = 22
        DueDateLabel.Text = "Due Date:"
        '
        'FineLabel
        '
        FineLabel.AutoSize = True
        FineLabel.Location = New System.Drawing.Point(13, 166)
        FineLabel.Name = "FineLabel"
        FineLabel.Size = New System.Drawing.Size(30, 13)
        FineLabel.TabIndex = 24
        FineLabel.Text = "Fine:"
        '
        'CustIDLabel
        '
        CustIDLabel.AutoSize = True
        CustIDLabel.Location = New System.Drawing.Point(24, 172)
        CustIDLabel.Name = "CustIDLabel"
        CustIDLabel.Size = New System.Drawing.Size(45, 13)
        CustIDLabel.TabIndex = 26
        CustIDLabel.Text = "Cust ID:"
        '
        'CustNameLabel
        '
        CustNameLabel.AutoSize = True
        CustNameLabel.Location = New System.Drawing.Point(24, 198)
        CustNameLabel.Name = "CustNameLabel"
        CustNameLabel.Size = New System.Drawing.Size(62, 13)
        CustNameLabel.TabIndex = 28
        CustNameLabel.Text = "Cust Name:"
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(24, 16)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(41, 13)
        IDLabel.TabIndex = 39
        IDLabel.Text = "ItemID:"
        '
        'NameLabel
        '
        NameLabel.AutoSize = True
        NameLabel.Location = New System.Drawing.Point(24, 42)
        NameLabel.Name = "NameLabel"
        NameLabel.Size = New System.Drawing.Size(38, 13)
        NameLabel.TabIndex = 41
        NameLabel.Text = "Name:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(17, 120)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(102, 13)
        Label1.TabIndex = 43
        Label1.Text = "Remaining Quantity:"
        '
        'QuantityLabel
        '
        QuantityLabel.AutoSize = True
        QuantityLabel.Location = New System.Drawing.Point(17, 96)
        QuantityLabel.Name = "QuantityLabel"
        QuantityLabel.Size = New System.Drawing.Size(49, 13)
        QuantityLabel.TabIndex = 44
        QuantityLabel.Text = "Quantity:"
        '
        'SizeTextBox
        '
        Me.SizeTextBox.Enabled = False
        Me.SizeTextBox.Location = New System.Drawing.Point(117, 65)
        Me.SizeTextBox.Name = "SizeTextBox"
        Me.SizeTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SizeTextBox.TabIndex = 5
        '
        'UnitPriceTextBox
        '
        Me.UnitPriceTextBox.Enabled = False
        Me.UnitPriceTextBox.Location = New System.Drawing.Point(117, 143)
        Me.UnitPriceTextBox.Name = "UnitPriceTextBox"
        Me.UnitPriceTextBox.Size = New System.Drawing.Size(200, 20)
        Me.UnitPriceTextBox.TabIndex = 9
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(354, 4)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(120, 45)
        Me.btnSave.TabIndex = 22
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(10, 10)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(511, 208)
        Me.DataGridView1.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.DataGridView1)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel5.Size = New System.Drawing.Size(571, 228)
        Me.Panel5.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(894, 456)
        Me.Panel1.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(323, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(571, 456)
        Me.Panel3.TabIndex = 1
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.DueDateDateTimePicker)
        Me.Panel4.Controls.Add(Me.btnSave)
        Me.Panel4.Controls.Add(ItemsSoldLabel1)
        Me.Panel4.Controls.Add(Me.FineTextBox)
        Me.Panel4.Controls.Add(Me.ItemsSoldTextBox)
        Me.Panel4.Controls.Add(FineLabel)
        Me.Panel4.Controls.Add(TotalPriceLabel1)
        Me.Panel4.Controls.Add(Me.TotalPriceTextBox)
        Me.Panel4.Controls.Add(DueDateLabel)
        Me.Panel4.Controls.Add(AmountDepositedLabel)
        Me.Panel4.Controls.Add(Me.AmountDueTextBox)
        Me.Panel4.Controls.Add(Me.AmountGivenTextBox)
        Me.Panel4.Controls.Add(AmountDueLabel)
        Me.Panel4.Controls.Add(DateDepositedLabel)
        Me.Panel4.Controls.Add(Me.DateDepositedDateTimePicker)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 228)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel4.Size = New System.Drawing.Size(571, 228)
        Me.Panel4.TabIndex = 1
        '
        'DueDateDateTimePicker
        '
        Me.DueDateDateTimePicker.CustomFormat = "dd-MMM-yyy"
        Me.DueDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DueDateDateTimePicker.Location = New System.Drawing.Point(116, 137)
        Me.DueDateDateTimePicker.Name = "DueDateDateTimePicker"
        Me.DueDateDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.DueDateDateTimePicker.TabIndex = 26
        '
        'FineTextBox
        '
        Me.FineTextBox.Location = New System.Drawing.Point(116, 163)
        Me.FineTextBox.Name = "FineTextBox"
        Me.FineTextBox.Size = New System.Drawing.Size(200, 20)
        Me.FineTextBox.TabIndex = 25
        '
        'ItemsSoldTextBox
        '
        Me.ItemsSoldTextBox.Location = New System.Drawing.Point(116, 7)
        Me.ItemsSoldTextBox.Name = "ItemsSoldTextBox"
        Me.ItemsSoldTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ItemsSoldTextBox.TabIndex = 13
        '
        'TotalPriceTextBox
        '
        Me.TotalPriceTextBox.Location = New System.Drawing.Point(116, 33)
        Me.TotalPriceTextBox.Name = "TotalPriceTextBox"
        Me.TotalPriceTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TotalPriceTextBox.TabIndex = 15
        '
        'AmountDueTextBox
        '
        Me.AmountDueTextBox.Location = New System.Drawing.Point(116, 111)
        Me.AmountDueTextBox.Name = "AmountDueTextBox"
        Me.AmountDueTextBox.Size = New System.Drawing.Size(200, 20)
        Me.AmountDueTextBox.TabIndex = 21
        '
        'AmountGivenTextBox
        '
        Me.AmountGivenTextBox.Location = New System.Drawing.Point(116, 59)
        Me.AmountGivenTextBox.Name = "AmountGivenTextBox"
        Me.AmountGivenTextBox.Size = New System.Drawing.Size(200, 20)
        Me.AmountGivenTextBox.TabIndex = 17
        '
        'DateDepositedDateTimePicker
        '
        Me.DateDepositedDateTimePicker.CustomFormat = "dd-MMM-yyy hh:mm:ss"
        Me.DateDepositedDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateDepositedDateTimePicker.Location = New System.Drawing.Point(116, 85)
        Me.DateDepositedDateTimePicker.Name = "DateDepositedDateTimePicker"
        Me.DateDepositedDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.DateDepositedDateTimePicker.TabIndex = 19
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.QuantityItemTextBox)
        Me.Panel2.Controls.Add(Label1)
        Me.Panel2.Controls.Add(QuantityLabel)
        Me.Panel2.Controls.Add(Me.QuantityTextBox)
        Me.Panel2.Controls.Add(Me.CustNameTextBox)
        Me.Panel2.Controls.Add(IDLabel)
        Me.Panel2.Controls.Add(Me.NameTextBox)
        Me.Panel2.Controls.Add(NameLabel)
        Me.Panel2.Controls.Add(Me.ItemIDComboBox)
        Me.Panel2.Controls.Add(Me.NewItemButton)
        Me.Panel2.Controls.Add(Me.btnAddToCart)
        Me.Panel2.Controls.Add(CustIDLabel)
        Me.Panel2.Controls.Add(CustNameLabel)
        Me.Panel2.Controls.Add(Me.CusIDComboBox)
        Me.Panel2.Controls.Add(SizeLabel)
        Me.Panel2.Controls.Add(Me.SizeTextBox)
        Me.Panel2.Controls.Add(UnitPriceLabel)
        Me.Panel2.Controls.Add(Me.UnitPriceTextBox)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(100, 10, 10, 10)
        Me.Panel2.Size = New System.Drawing.Size(375, 456)
        Me.Panel2.TabIndex = 0
        '
        'QuantityItemTextBox
        '
        Me.QuantityItemTextBox.Enabled = False
        Me.QuantityItemTextBox.Location = New System.Drawing.Point(117, 117)
        Me.QuantityItemTextBox.Name = "QuantityItemTextBox"
        Me.QuantityItemTextBox.Size = New System.Drawing.Size(200, 20)
        Me.QuantityItemTextBox.TabIndex = 46
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.Location = New System.Drawing.Point(117, 91)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(200, 20)
        Me.QuantityTextBox.TabIndex = 45
        Me.QuantityTextBox.Text = "0"
        '
        'CustNameTextBox
        '
        Me.CustNameTextBox.Location = New System.Drawing.Point(117, 195)
        Me.CustNameTextBox.Name = "CustNameTextBox"
        Me.CustNameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CustNameTextBox.TabIndex = 27
        '
        'NameTextBox
        '
        Me.NameTextBox.Enabled = False
        Me.NameTextBox.Location = New System.Drawing.Point(117, 40)
        Me.NameTextBox.Name = "NameTextBox"
        Me.NameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.NameTextBox.TabIndex = 40
        '
        'ItemIDComboBox
        '
        Me.ItemIDComboBox.FormattingEnabled = True
        Me.ItemIDComboBox.Location = New System.Drawing.Point(117, 13)
        Me.ItemIDComboBox.Name = "ItemIDComboBox"
        Me.ItemIDComboBox.Size = New System.Drawing.Size(200, 21)
        Me.ItemIDComboBox.TabIndex = 42
        Me.ItemIDComboBox.Text = "--Select Item ID--"
        '
        'NewItemButton
        '
        Me.NewItemButton.Location = New System.Drawing.Point(27, 232)
        Me.NewItemButton.Name = "NewItemButton"
        Me.NewItemButton.Size = New System.Drawing.Size(122, 45)
        Me.NewItemButton.TabIndex = 38
        Me.NewItemButton.Text = "New Sale"
        Me.NewItemButton.UseVisualStyleBackColor = True
        '
        'btnAddToCart
        '
        Me.btnAddToCart.Location = New System.Drawing.Point(174, 232)
        Me.btnAddToCart.Name = "btnAddToCart"
        Me.btnAddToCart.Size = New System.Drawing.Size(120, 45)
        Me.btnAddToCart.TabIndex = 37
        Me.btnAddToCart.Text = "AddToCart"
        Me.btnAddToCart.UseVisualStyleBackColor = True
        '
        'CusIDComboBox
        '
        Me.CusIDComboBox.FormattingEnabled = True
        Me.CusIDComboBox.Location = New System.Drawing.Point(117, 169)
        Me.CusIDComboBox.Name = "CusIDComboBox"
        Me.CusIDComboBox.Size = New System.Drawing.Size(200, 21)
        Me.CusIDComboBox.TabIndex = 29
        Me.CusIDComboBox.Text = "--Select Customer ID--"
        '
        'PrintForm1
        '
        Me.PrintForm1.DocumentName = "document"
        Me.PrintForm1.Form = Me
        Me.PrintForm1.PrintAction = System.Drawing.Printing.PrintAction.PrintToPrinter
        Me.PrintForm1.PrinterSettings = CType(resources.GetObject("PrintForm1.PrinterSettings"), System.Drawing.Printing.PrinterSettings)
        Me.PrintForm1.PrintFileName = Nothing
        '
        'frmDepositItemSale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 456)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmDepositItemSale"
        Me.Text = "frmDepositItemSale"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SizeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UnitPriceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents FineTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemsSoldTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TotalPriceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AmountDueTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AmountGivenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DateDepositedDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents CustNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CusIDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents NewItemButton As System.Windows.Forms.Button
    Friend WithEvents btnAddToCart As System.Windows.Forms.Button
    Friend WithEvents NameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemIDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PrintForm1 As Microsoft.VisualBasic.PowerPacks.Printing.PrintForm
    Friend WithEvents DueDateDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents QuantityItemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents QuantityTextBox As System.Windows.Forms.TextBox
End Class
