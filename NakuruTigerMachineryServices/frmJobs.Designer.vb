﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJobs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim EmpIDLabel As System.Windows.Forms.Label
        Dim EmpNameLabel As System.Windows.Forms.Label
        Dim StartDateLabel As System.Windows.Forms.Label
        Dim EndDateLabel As System.Windows.Forms.Label
        Dim WageLabel As System.Windows.Forms.Label
        Dim IDLabel1 As System.Windows.Forms.Label
        Dim SizeLabel As System.Windows.Forms.Label
        Dim QuantityLabel As System.Windows.Forms.Label
        Dim UnitPriceLabel As System.Windows.Forms.Label
        Dim NameLabel1 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim JobNameLabel1 As System.Windows.Forms.Label
        Dim IDLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.JobDescriptionTextBox = New System.Windows.Forms.TextBox()
        Me.IDTextBox = New System.Windows.Forms.TextBox()
        Me.JobNameIDComboBox = New System.Windows.Forms.ComboBox()
        Me.JobNameTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.StartDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.EmpNameTextBox = New System.Windows.Forms.TextBox()
        Me.EmpIDComboBox = New System.Windows.Forms.ComboBox()
        Me.EndDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.WageTextBox = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.quantityItemTextBox = New System.Windows.Forms.TextBox()
        Me.InventionIDTextBox = New System.Windows.Forms.TextBox()
        Me.ItemIDComboBox = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.NameTextBox = New System.Windows.Forms.TextBox()
        Me.UnitPriceTextBox = New System.Windows.Forms.TextBox()
        Me.SizeTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        EmpIDLabel = New System.Windows.Forms.Label()
        EmpNameLabel = New System.Windows.Forms.Label()
        StartDateLabel = New System.Windows.Forms.Label()
        EndDateLabel = New System.Windows.Forms.Label()
        WageLabel = New System.Windows.Forms.Label()
        IDLabel1 = New System.Windows.Forms.Label()
        SizeLabel = New System.Windows.Forms.Label()
        QuantityLabel = New System.Windows.Forms.Label()
        UnitPriceLabel = New System.Windows.Forms.Label()
        NameLabel1 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        JobNameLabel1 = New System.Windows.Forms.Label()
        IDLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Me.Panel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'EmpIDLabel
        '
        EmpIDLabel.AutoSize = True
        EmpIDLabel.Location = New System.Drawing.Point(9, 136)
        EmpIDLabel.Name = "EmpIDLabel"
        EmpIDLabel.Size = New System.Drawing.Size(45, 13)
        EmpIDLabel.TabIndex = 14
        EmpIDLabel.Text = "Emp ID:"
        '
        'EmpNameLabel
        '
        EmpNameLabel.AutoSize = True
        EmpNameLabel.Location = New System.Drawing.Point(9, 163)
        EmpNameLabel.Name = "EmpNameLabel"
        EmpNameLabel.Size = New System.Drawing.Size(62, 13)
        EmpNameLabel.TabIndex = 16
        EmpNameLabel.Text = "Emp Name:"
        '
        'StartDateLabel
        '
        StartDateLabel.AutoSize = True
        StartDateLabel.Location = New System.Drawing.Point(9, 190)
        StartDateLabel.Name = "StartDateLabel"
        StartDateLabel.Size = New System.Drawing.Size(58, 13)
        StartDateLabel.TabIndex = 18
        StartDateLabel.Text = "Start Date:"
        '
        'EndDateLabel
        '
        EndDateLabel.AutoSize = True
        EndDateLabel.Location = New System.Drawing.Point(17, 169)
        EndDateLabel.Name = "EndDateLabel"
        EndDateLabel.Size = New System.Drawing.Size(55, 13)
        EndDateLabel.TabIndex = 20
        EndDateLabel.Text = "End Date:"
        '
        'WageLabel
        '
        WageLabel.AutoSize = True
        WageLabel.Location = New System.Drawing.Point(17, 194)
        WageLabel.Name = "WageLabel"
        WageLabel.Size = New System.Drawing.Size(39, 13)
        WageLabel.TabIndex = 22
        WageLabel.Text = "Wage:"
        '
        'IDLabel1
        '
        IDLabel1.AutoSize = True
        IDLabel1.Location = New System.Drawing.Point(17, 39)
        IDLabel1.Name = "IDLabel1"
        IDLabel1.Size = New System.Drawing.Size(44, 13)
        IDLabel1.TabIndex = 22
        IDLabel1.Text = "Item ID:"
        '
        'SizeLabel
        '
        SizeLabel.AutoSize = True
        SizeLabel.Location = New System.Drawing.Point(17, 91)
        SizeLabel.Name = "SizeLabel"
        SizeLabel.Size = New System.Drawing.Size(91, 13)
        SizeLabel.TabIndex = 26
        SizeLabel.Text = "Size/ Description:"
        '
        'QuantityLabel
        '
        QuantityLabel.AutoSize = True
        QuantityLabel.Location = New System.Drawing.Point(17, 117)
        QuantityLabel.Name = "QuantityLabel"
        QuantityLabel.Size = New System.Drawing.Size(49, 13)
        QuantityLabel.TabIndex = 28
        QuantityLabel.Text = "Quantity:"
        '
        'UnitPriceLabel
        '
        UnitPriceLabel.AutoSize = True
        UnitPriceLabel.Location = New System.Drawing.Point(17, 143)
        UnitPriceLabel.Name = "UnitPriceLabel"
        UnitPriceLabel.Size = New System.Drawing.Size(56, 13)
        UnitPriceLabel.TabIndex = 30
        UnitPriceLabel.Text = "Unit Price:"
        '
        'NameLabel1
        '
        NameLabel1.AutoSize = True
        NameLabel1.Location = New System.Drawing.Point(17, 65)
        NameLabel1.Name = "NameLabel1"
        NameLabel1.Size = New System.Drawing.Size(61, 13)
        NameLabel1.TabIndex = 31
        NameLabel1.Text = "Item Name:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(9, 58)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(72, 13)
        Label1.TabIndex = 25
        Label1.Text = "Job Name ID:"
        '
        'JobNameLabel1
        '
        JobNameLabel1.AutoSize = True
        JobNameLabel1.Location = New System.Drawing.Point(9, 82)
        JobNameLabel1.Name = "JobNameLabel1"
        JobNameLabel1.Size = New System.Drawing.Size(58, 13)
        JobNameLabel1.TabIndex = 26
        JobNameLabel1.Text = "Job Name:"
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(9, 32)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(21, 13)
        IDLabel.TabIndex = 27
        IDLabel.Text = "ID:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(448, 39)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(68, 13)
        Label2.TabIndex = 34
        Label2.Text = "Invention ID:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Location = New System.Drawing.Point(9, 110)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(83, 13)
        Label3.TabIndex = 14
        Label3.Text = "Job Description:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.GroupBox1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(10)
        Me.Panel2.Size = New System.Drawing.Size(434, 490)
        Me.Panel2.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.JobDescriptionTextBox)
        Me.GroupBox1.Controls.Add(IDLabel)
        Me.GroupBox1.Controls.Add(Me.IDTextBox)
        Me.GroupBox1.Controls.Add(JobNameLabel1)
        Me.GroupBox1.Controls.Add(Me.JobNameIDComboBox)
        Me.GroupBox1.Controls.Add(Label1)
        Me.GroupBox1.Controls.Add(Me.JobNameTextBox)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.StartDateDateTimePicker)
        Me.GroupBox1.Controls.Add(StartDateLabel)
        Me.GroupBox1.Controls.Add(Me.EmpNameTextBox)
        Me.GroupBox1.Controls.Add(EmpNameLabel)
        Me.GroupBox1.Controls.Add(Me.EmpIDComboBox)
        Me.GroupBox1.Controls.Add(Label3)
        Me.GroupBox1.Controls.Add(EmpIDLabel)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(10, 10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(414, 470)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Item Invention"
        '
        'JobDescriptionTextBox
        '
        Me.JobDescriptionTextBox.Enabled = False
        Me.JobDescriptionTextBox.Location = New System.Drawing.Point(94, 107)
        Me.JobDescriptionTextBox.Name = "JobDescriptionTextBox"
        Me.JobDescriptionTextBox.Size = New System.Drawing.Size(200, 20)
        Me.JobDescriptionTextBox.TabIndex = 29
        '
        'IDTextBox
        '
        Me.IDTextBox.Enabled = False
        Me.IDTextBox.Location = New System.Drawing.Point(94, 29)
        Me.IDTextBox.Name = "IDTextBox"
        Me.IDTextBox.Size = New System.Drawing.Size(200, 20)
        Me.IDTextBox.TabIndex = 28
        '
        'JobNameIDComboBox
        '
        Me.JobNameIDComboBox.FormattingEnabled = True
        Me.JobNameIDComboBox.Location = New System.Drawing.Point(94, 55)
        Me.JobNameIDComboBox.Name = "JobNameIDComboBox"
        Me.JobNameIDComboBox.Size = New System.Drawing.Size(200, 21)
        Me.JobNameIDComboBox.TabIndex = 27
        Me.JobNameIDComboBox.Tag = ""
        Me.JobNameIDComboBox.Text = "--Select Job Name ID--"
        '
        'JobNameTextBox
        '
        Me.JobNameTextBox.Enabled = False
        Me.JobNameTextBox.Location = New System.Drawing.Point(94, 82)
        Me.JobNameTextBox.Name = "JobNameTextBox"
        Me.JobNameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.JobNameTextBox.TabIndex = 26
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(94, 230)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(194, 41)
        Me.Button1.TabIndex = 24
        Me.Button1.Text = "Start Job"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'StartDateDateTimePicker
        '
        Me.StartDateDateTimePicker.CustomFormat = "dd-MMM-yyy"
        Me.StartDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.StartDateDateTimePicker.Location = New System.Drawing.Point(94, 186)
        Me.StartDateDateTimePicker.Name = "StartDateDateTimePicker"
        Me.StartDateDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.StartDateDateTimePicker.TabIndex = 19
        '
        'EmpNameTextBox
        '
        Me.EmpNameTextBox.Enabled = False
        Me.EmpNameTextBox.Location = New System.Drawing.Point(94, 160)
        Me.EmpNameTextBox.Name = "EmpNameTextBox"
        Me.EmpNameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.EmpNameTextBox.TabIndex = 17
        '
        'EmpIDComboBox
        '
        Me.EmpIDComboBox.FormattingEnabled = True
        Me.EmpIDComboBox.Location = New System.Drawing.Point(94, 133)
        Me.EmpIDComboBox.Name = "EmpIDComboBox"
        Me.EmpIDComboBox.Size = New System.Drawing.Size(200, 21)
        Me.EmpIDComboBox.TabIndex = 15
        Me.EmpIDComboBox.Text = "--Select Employee ID--"
        '
        'EndDateDateTimePicker
        '
        Me.EndDateDateTimePicker.CustomFormat = "dd-MMM-yyy"
        Me.EndDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.EndDateDateTimePicker.Location = New System.Drawing.Point(112, 166)
        Me.EndDateDateTimePicker.Name = "EndDateDateTimePicker"
        Me.EndDateDateTimePicker.Size = New System.Drawing.Size(225, 20)
        Me.EndDateDateTimePicker.TabIndex = 21
        '
        'WageTextBox
        '
        Me.WageTextBox.Location = New System.Drawing.Point(112, 192)
        Me.WageTextBox.Name = "WageTextBox"
        Me.WageTextBox.Size = New System.Drawing.Size(225, 20)
        Me.WageTextBox.TabIndex = 23
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(10, 10)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(864, 214)
        Me.DataGridView1.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.DataGridView1)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel5.Size = New System.Drawing.Size(924, 234)
        Me.Panel5.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1354, 490)
        Me.Panel1.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.AutoScroll = True
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(430, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(924, 490)
        Me.Panel3.TabIndex = 1
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.GroupBox2)
        Me.Panel4.Location = New System.Drawing.Point(0, 240)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel4.Size = New System.Drawing.Size(924, 233)
        Me.Panel4.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.quantityItemTextBox)
        Me.GroupBox2.Controls.Add(Label2)
        Me.GroupBox2.Controls.Add(Me.InventionIDTextBox)
        Me.GroupBox2.Controls.Add(NameLabel1)
        Me.GroupBox2.Controls.Add(Me.ItemIDComboBox)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(IDLabel1)
        Me.GroupBox2.Controls.Add(WageLabel)
        Me.GroupBox2.Controls.Add(Me.NameTextBox)
        Me.GroupBox2.Controls.Add(Me.WageTextBox)
        Me.GroupBox2.Controls.Add(Me.EndDateDateTimePicker)
        Me.GroupBox2.Controls.Add(EndDateLabel)
        Me.GroupBox2.Controls.Add(SizeLabel)
        Me.GroupBox2.Controls.Add(Me.UnitPriceTextBox)
        Me.GroupBox2.Controls.Add(Me.SizeTextBox)
        Me.GroupBox2.Controls.Add(UnitPriceLabel)
        Me.GroupBox2.Controls.Add(QuantityLabel)
        Me.GroupBox2.Controls.Add(Me.QuantityTextBox)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(10, 10)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(864, 213)
        Me.GroupBox2.TabIndex = 32
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Final Product"
        '
        'quantityItemTextBox
        '
        Me.quantityItemTextBox.Enabled = False
        Me.quantityItemTextBox.Location = New System.Drawing.Point(112, 114)
        Me.quantityItemTextBox.Name = "quantityItemTextBox"
        Me.quantityItemTextBox.Size = New System.Drawing.Size(109, 20)
        Me.quantityItemTextBox.TabIndex = 36
        Me.quantityItemTextBox.Tag = "0"
        Me.quantityItemTextBox.Text = "0"
        '
        'InventionIDTextBox
        '
        Me.InventionIDTextBox.Location = New System.Drawing.Point(518, 35)
        Me.InventionIDTextBox.Name = "InventionIDTextBox"
        Me.InventionIDTextBox.Size = New System.Drawing.Size(110, 20)
        Me.InventionIDTextBox.TabIndex = 35
        '
        'ItemIDComboBox
        '
        Me.ItemIDComboBox.FormattingEnabled = True
        Me.ItemIDComboBox.Location = New System.Drawing.Point(112, 35)
        Me.ItemIDComboBox.Name = "ItemIDComboBox"
        Me.ItemIDComboBox.Size = New System.Drawing.Size(225, 21)
        Me.ItemIDComboBox.TabIndex = 32
        Me.ItemIDComboBox.Text = "--Select Item ID--"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(451, 70)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(177, 41)
        Me.Button2.TabIndex = 25
        Me.Button2.Text = "Finish Job"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'NameTextBox
        '
        Me.NameTextBox.Enabled = False
        Me.NameTextBox.Location = New System.Drawing.Point(112, 62)
        Me.NameTextBox.Name = "NameTextBox"
        Me.NameTextBox.Size = New System.Drawing.Size(225, 20)
        Me.NameTextBox.TabIndex = 23
        '
        'UnitPriceTextBox
        '
        Me.UnitPriceTextBox.Location = New System.Drawing.Point(112, 140)
        Me.UnitPriceTextBox.Name = "UnitPriceTextBox"
        Me.UnitPriceTextBox.Size = New System.Drawing.Size(225, 20)
        Me.UnitPriceTextBox.TabIndex = 31
        '
        'SizeTextBox
        '
        Me.SizeTextBox.Enabled = False
        Me.SizeTextBox.Location = New System.Drawing.Point(112, 88)
        Me.SizeTextBox.Name = "SizeTextBox"
        Me.SizeTextBox.Size = New System.Drawing.Size(225, 20)
        Me.SizeTextBox.TabIndex = 27
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.Location = New System.Drawing.Point(227, 114)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(110, 20)
        Me.QuantityTextBox.TabIndex = 29
        '
        'frmJobs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1354, 490)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmJobs"
        Me.Text = "frmJobs"
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents EmpIDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents EmpNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StartDateDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents EndDateDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents WageTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents NameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SizeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents QuantityTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UnitPriceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ItemIDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents JobNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents JobNameIDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents IDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents InventionIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents quantityItemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents JobDescriptionTextBox As System.Windows.Forms.TextBox
End Class
