﻿Imports MySql.Data.MySqlClient
Public Class frmTools
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader

    Private Sub loadtable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.Tools"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub searchTable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.Tools Where ToolName like '%" + txtSearch.Text + "%'"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub

    Private Sub clearTools()
        ToolNameTextBox.Clear()
        SizeTextBox.Clear()
        QuantityTextBox.Clear()
        ToolValueTextBox.Clear()

    End Sub
    Private Sub saveTools()
        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.Tools ( ToolName,Size , UnitValue, Quantity, ToolValue) Values (  '" & ToolNameTextBox.Text & "',  '" & SizeTextBox.Text & "', '" & UnitValueTextBox.Text & "', '" & QuantityTextBox.Text & "',     '" & ToolValueTextBox.Text & "'  )"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If ToolNameTextBox.Text = "" Then
                MsgBox("Please Enter Tool Name", MsgBoxStyle.Critical, "warning")
                ToolNameTextBox.Focus()
            ElseIf SizeTextBox.Text = "" Then
                MsgBox("Please Enter Tool Size", MsgBoxStyle.Critical, "warning")
                SizeTextBox.Focus()
            ElseIf UnitValueTextBox.Text = "" Then
                MsgBox("Please Enter Value of one Tool ", MsgBoxStyle.Critical, "warning")
                UnitValueTextBox.Focus()
            ElseIf QuantityTextBox.Text = "" Then
                MsgBox("Please Enter Number of Tools", MsgBoxStyle.Critical, "warning")
                QuantityTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()
                MsgBox("Save Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub updateTools()
        Try

            Call openconn()
            Dim query As String
            query = "Update ntmsdata.Tools set  ToolName='" & ToolNameTextBox.Text & "', Size = '" & SizeTextBox.Text & "', UnitValue ='" & UnitValueTextBox.Text & "',  Quantity ='" & QuantityTextBox.Text & "',     ToolValue ='" & ToolValueTextBox.Text & "' where ID ='" & IDTextBox.Text & "'"
            cmd = New MySqlCommand(query, con)

            cmd.Connection = con

            If ToolNameTextBox.Text = "" Then
                MsgBox("Please Enter Tool Name", MsgBoxStyle.Critical, "warning")
                ToolNameTextBox.Focus()
            ElseIf SizeTextBox.Text = "" Then
                MsgBox("Please Enter Tool Size", MsgBoxStyle.Critical, "warning")
                SizeTextBox.Focus()
            ElseIf UnitValueTextBox.Text = "" Then
                MsgBox("Please Enter Value of one Tool ", MsgBoxStyle.Critical, "warning")
                UnitValueTextBox.Focus()
            ElseIf QuantityTextBox.Text = "" Then
                MsgBox("Please Enter Number of Tools", MsgBoxStyle.Critical, "warning")
                QuantityTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()

                MsgBox("Update Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub deleteTools()
        Try
            If MessageBox.Show("Do you really want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then

                Call openconn()
                Dim query As String
                query = "delete from ntmsdata.Tools where ID ='" & IDTextBox.Text & "'  "
                cmd = New MySqlCommand(query, con)
                reader = cmd.ExecuteReader
                MsgBox("Delete Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        clearTools()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Call saveTools()

    End Sub


    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Call deleteTools()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Call updateTools()

    End Sub

    Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Call loadtable()
        Call searchTable()
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If e.RowIndex >= 0 Then
            'gets a collection that contains all the rows
            Dim row As DataGridViewRow = Me.DataGridView1.Rows(e.RowIndex)
            'populate the textbox from specific value of the coordinates of column and row.


            IDTextBox.Text = row.Cells(0).Value.ToString()
            ToolNameTextBox.Text = row.Cells(1).Value.ToString()
            SizeTextBox.Text = row.Cells(2).Value.ToString()
            UnitValueTextBox.Text = row.Cells(3).Value.ToString()
            QuantityTextBox.Text = row.Cells(4).Value.ToString()
            ToolValueTextBox.Text = row.Cells(5).Value.ToString()

            If IDTextBox.Text = "" Then
                MsgBox("Please Select A row with data on the datagrid View below", MsgBoxStyle.Critical)

            End If

        End If
    End Sub

    Private Sub QuantityTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles QuantityTextBox.KeyPress
        Dim NotAllowed As String = "0123456789"
        If e.KeyChar <> ControlChars.Back Then
            If NotAllowed.IndexOf(e.KeyChar) = -1 Then
                e.Handled = True
                MsgBox("Letters not allowed...", MsgBoxStyle.Critical, "Attention....")
            End If
        End If
    End Sub

    Private Sub UnitValueTextBox_TextChanged(sender As Object, e As EventArgs) Handles UnitValueTextBox.TextChanged
        ToolValueTextBox.Text = (Val(QuantityTextBox.Text)) * (Val(UnitValueTextBox.Text))
    End Sub

    Private Sub UnitValueTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles UnitValueTextBox.KeyPress
        Dim NotAllowed As String = "0123456789"
        If e.KeyChar <> ControlChars.Back Then
            If NotAllowed.IndexOf(e.KeyChar) = -1 Then
                e.Handled = True
                MsgBox("Letters not allowed...", MsgBoxStyle.Critical, "Attention....")
            End If
        End If
    End Sub

    Private Sub QuantityTextBox_TextChanged(sender As Object, e As EventArgs) Handles QuantityTextBox.TextChanged
        ToolValueTextBox.Text = (Val(QuantityTextBox.Text)) * (Val(UnitValueTextBox.Text))

    End Sub

    Private Sub frmTools_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadtable()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class