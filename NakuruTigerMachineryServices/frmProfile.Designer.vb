﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProfile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim IDLabel As System.Windows.Forms.Label
        Dim CompanyNameLabel As System.Windows.Forms.Label
        Dim AddressLabel As System.Windows.Forms.Label
        Dim EMail1Label As System.Windows.Forms.Label
        Dim EMail2Label As System.Windows.Forms.Label
        Dim EMail3Label As System.Windows.Forms.Label
        Dim Telephone1Label As System.Windows.Forms.Label
        Dim Telephone2Label As System.Windows.Forms.Label
        Dim Telephone3Label As System.Windows.Forms.Label
        Dim WebsiteLabel As System.Windows.Forms.Label
        Dim FacebookLabel As System.Windows.Forms.Label
        Dim TwitterLabel As System.Windows.Forms.Label
        Me.IDTextBox = New System.Windows.Forms.TextBox()
        Me.CompanyNameTextBox = New System.Windows.Forms.TextBox()
        Me.AddressTextBox = New System.Windows.Forms.TextBox()
        Me.EMail1TextBox = New System.Windows.Forms.TextBox()
        Me.EMail2TextBox = New System.Windows.Forms.TextBox()
        Me.EMail3TextBox = New System.Windows.Forms.TextBox()
        Me.Telephone1TextBox = New System.Windows.Forms.TextBox()
        Me.Telephone2TextBox = New System.Windows.Forms.TextBox()
        Me.Telephone3TextBox = New System.Windows.Forms.TextBox()
        Me.WebsiteTextBox = New System.Windows.Forms.TextBox()
        Me.FacebookTextBox = New System.Windows.Forms.TextBox()
        Me.TwitterTextBox = New System.Windows.Forms.TextBox()
        IDLabel = New System.Windows.Forms.Label()
        CompanyNameLabel = New System.Windows.Forms.Label()
        AddressLabel = New System.Windows.Forms.Label()
        EMail1Label = New System.Windows.Forms.Label()
        EMail2Label = New System.Windows.Forms.Label()
        EMail3Label = New System.Windows.Forms.Label()
        Telephone1Label = New System.Windows.Forms.Label()
        Telephone2Label = New System.Windows.Forms.Label()
        Telephone3Label = New System.Windows.Forms.Label()
        WebsiteLabel = New System.Windows.Forms.Label()
        FacebookLabel = New System.Windows.Forms.Label()
        TwitterLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(23, 15)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(21, 13)
        IDLabel.TabIndex = 1
        IDLabel.Text = "ID:"
        '
        'IDTextBox
        '
        Me.IDTextBox.Location = New System.Drawing.Point(114, 12)
        Me.IDTextBox.Name = "IDTextBox"
        Me.IDTextBox.Size = New System.Drawing.Size(100, 20)
        Me.IDTextBox.TabIndex = 2
        '
        'CompanyNameLabel
        '
        CompanyNameLabel.AutoSize = True
        CompanyNameLabel.Location = New System.Drawing.Point(23, 41)
        CompanyNameLabel.Name = "CompanyNameLabel"
        CompanyNameLabel.Size = New System.Drawing.Size(85, 13)
        CompanyNameLabel.TabIndex = 3
        CompanyNameLabel.Text = "Company Name:"
        '
        'CompanyNameTextBox
        '
        Me.CompanyNameTextBox.Location = New System.Drawing.Point(114, 38)
        Me.CompanyNameTextBox.Name = "CompanyNameTextBox"
        Me.CompanyNameTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CompanyNameTextBox.TabIndex = 4
        '
        'AddressLabel
        '
        AddressLabel.AutoSize = True
        AddressLabel.Location = New System.Drawing.Point(23, 67)
        AddressLabel.Name = "AddressLabel"
        AddressLabel.Size = New System.Drawing.Size(48, 13)
        AddressLabel.TabIndex = 5
        AddressLabel.Text = "Address:"
        '
        'AddressTextBox
        '
        Me.AddressTextBox.Location = New System.Drawing.Point(114, 64)
        Me.AddressTextBox.Name = "AddressTextBox"
        Me.AddressTextBox.Size = New System.Drawing.Size(100, 20)
        Me.AddressTextBox.TabIndex = 6
        '
        'EMail1Label
        '
        EMail1Label.AutoSize = True
        EMail1Label.Location = New System.Drawing.Point(23, 93)
        EMail1Label.Name = "EMail1Label"
        EMail1Label.Size = New System.Drawing.Size(42, 13)
        EMail1Label.TabIndex = 7
        EMail1Label.Text = "EMail1:"
        '
        'EMail1TextBox
        '
        Me.EMail1TextBox.Location = New System.Drawing.Point(114, 90)
        Me.EMail1TextBox.Name = "EMail1TextBox"
        Me.EMail1TextBox.Size = New System.Drawing.Size(100, 20)
        Me.EMail1TextBox.TabIndex = 8
        '
        'EMail2Label
        '
        EMail2Label.AutoSize = True
        EMail2Label.Location = New System.Drawing.Point(23, 119)
        EMail2Label.Name = "EMail2Label"
        EMail2Label.Size = New System.Drawing.Size(42, 13)
        EMail2Label.TabIndex = 9
        EMail2Label.Text = "EMail2:"
        '
        'EMail2TextBox
        '
        Me.EMail2TextBox.Location = New System.Drawing.Point(114, 116)
        Me.EMail2TextBox.Name = "EMail2TextBox"
        Me.EMail2TextBox.Size = New System.Drawing.Size(100, 20)
        Me.EMail2TextBox.TabIndex = 10
        '
        'EMail3Label
        '
        EMail3Label.AutoSize = True
        EMail3Label.Location = New System.Drawing.Point(23, 145)
        EMail3Label.Name = "EMail3Label"
        EMail3Label.Size = New System.Drawing.Size(42, 13)
        EMail3Label.TabIndex = 11
        EMail3Label.Text = "EMail3:"
        '
        'EMail3TextBox
        '
        Me.EMail3TextBox.Location = New System.Drawing.Point(114, 142)
        Me.EMail3TextBox.Name = "EMail3TextBox"
        Me.EMail3TextBox.Size = New System.Drawing.Size(100, 20)
        Me.EMail3TextBox.TabIndex = 12
        '
        'Telephone1Label
        '
        Telephone1Label.AutoSize = True
        Telephone1Label.Location = New System.Drawing.Point(23, 171)
        Telephone1Label.Name = "Telephone1Label"
        Telephone1Label.Size = New System.Drawing.Size(67, 13)
        Telephone1Label.TabIndex = 13
        Telephone1Label.Text = "Telephone1:"
        '
        'Telephone1TextBox
        '
        Me.Telephone1TextBox.Location = New System.Drawing.Point(114, 168)
        Me.Telephone1TextBox.Name = "Telephone1TextBox"
        Me.Telephone1TextBox.Size = New System.Drawing.Size(100, 20)
        Me.Telephone1TextBox.TabIndex = 14
        '
        'Telephone2Label
        '
        Telephone2Label.AutoSize = True
        Telephone2Label.Location = New System.Drawing.Point(23, 197)
        Telephone2Label.Name = "Telephone2Label"
        Telephone2Label.Size = New System.Drawing.Size(67, 13)
        Telephone2Label.TabIndex = 15
        Telephone2Label.Text = "Telephone2:"
        '
        'Telephone2TextBox
        '
        Me.Telephone2TextBox.Location = New System.Drawing.Point(114, 194)
        Me.Telephone2TextBox.Name = "Telephone2TextBox"
        Me.Telephone2TextBox.Size = New System.Drawing.Size(100, 20)
        Me.Telephone2TextBox.TabIndex = 16
        '
        'Telephone3Label
        '
        Telephone3Label.AutoSize = True
        Telephone3Label.Location = New System.Drawing.Point(23, 223)
        Telephone3Label.Name = "Telephone3Label"
        Telephone3Label.Size = New System.Drawing.Size(67, 13)
        Telephone3Label.TabIndex = 17
        Telephone3Label.Text = "Telephone3:"
        '
        'Telephone3TextBox
        '
        Me.Telephone3TextBox.Location = New System.Drawing.Point(114, 220)
        Me.Telephone3TextBox.Name = "Telephone3TextBox"
        Me.Telephone3TextBox.Size = New System.Drawing.Size(100, 20)
        Me.Telephone3TextBox.TabIndex = 18
        '
        'WebsiteLabel
        '
        WebsiteLabel.AutoSize = True
        WebsiteLabel.Location = New System.Drawing.Point(23, 249)
        WebsiteLabel.Name = "WebsiteLabel"
        WebsiteLabel.Size = New System.Drawing.Size(49, 13)
        WebsiteLabel.TabIndex = 19
        WebsiteLabel.Text = "Website:"
        '
        'WebsiteTextBox
        '
        Me.WebsiteTextBox.Location = New System.Drawing.Point(114, 246)
        Me.WebsiteTextBox.Name = "WebsiteTextBox"
        Me.WebsiteTextBox.Size = New System.Drawing.Size(100, 20)
        Me.WebsiteTextBox.TabIndex = 20
        '
        'FacebookLabel
        '
        FacebookLabel.AutoSize = True
        FacebookLabel.Location = New System.Drawing.Point(23, 275)
        FacebookLabel.Name = "FacebookLabel"
        FacebookLabel.Size = New System.Drawing.Size(58, 13)
        FacebookLabel.TabIndex = 21
        FacebookLabel.Text = "Facebook:"
        '
        'FacebookTextBox
        '
        Me.FacebookTextBox.Location = New System.Drawing.Point(114, 272)
        Me.FacebookTextBox.Name = "FacebookTextBox"
        Me.FacebookTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FacebookTextBox.TabIndex = 22
        '
        'TwitterLabel
        '
        TwitterLabel.AutoSize = True
        TwitterLabel.Location = New System.Drawing.Point(23, 301)
        TwitterLabel.Name = "TwitterLabel"
        TwitterLabel.Size = New System.Drawing.Size(42, 13)
        TwitterLabel.TabIndex = 23
        TwitterLabel.Text = "Twitter:"
        '
        'TwitterTextBox
        '
        Me.TwitterTextBox.Location = New System.Drawing.Point(114, 298)
        Me.TwitterTextBox.Name = "TwitterTextBox"
        Me.TwitterTextBox.Size = New System.Drawing.Size(100, 20)
        Me.TwitterTextBox.TabIndex = 24
        '
        'frmProfile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(847, 602)
        Me.Controls.Add(IDLabel)
        Me.Controls.Add(Me.IDTextBox)
        Me.Controls.Add(CompanyNameLabel)
        Me.Controls.Add(Me.CompanyNameTextBox)
        Me.Controls.Add(AddressLabel)
        Me.Controls.Add(Me.AddressTextBox)
        Me.Controls.Add(EMail1Label)
        Me.Controls.Add(Me.EMail1TextBox)
        Me.Controls.Add(EMail2Label)
        Me.Controls.Add(Me.EMail2TextBox)
        Me.Controls.Add(EMail3Label)
        Me.Controls.Add(Me.EMail3TextBox)
        Me.Controls.Add(Telephone1Label)
        Me.Controls.Add(Me.Telephone1TextBox)
        Me.Controls.Add(Telephone2Label)
        Me.Controls.Add(Me.Telephone2TextBox)
        Me.Controls.Add(Telephone3Label)
        Me.Controls.Add(Me.Telephone3TextBox)
        Me.Controls.Add(WebsiteLabel)
        Me.Controls.Add(Me.WebsiteTextBox)
        Me.Controls.Add(FacebookLabel)
        Me.Controls.Add(Me.FacebookTextBox)
        Me.Controls.Add(TwitterLabel)
        Me.Controls.Add(Me.TwitterTextBox)
        Me.Name = "frmProfile"
        Me.Text = "frmProfile"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents IDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CompanyNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AddressTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EMail1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents EMail2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents EMail3TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Telephone1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Telephone2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Telephone3TextBox As System.Windows.Forms.TextBox
    Friend WithEvents WebsiteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FacebookTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TwitterTextBox As System.Windows.Forms.TextBox
End Class
