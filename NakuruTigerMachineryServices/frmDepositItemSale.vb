﻿
Imports MySql.Data.MySqlClient
Public Class frmDepositItemSale
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim table As New DataTable
    Private TotalPrice As String
    Private AmountGiven As String
    Private Balance As String
    Private Sub loadItemstable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.Items"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub AddCart()
        ' Dim dalpsource As New BindingSource
        table.Columns.Add("ID", GetType(String))
        table.Columns.Add("Name", GetType(String))
        table.Columns.Add("Size/Description", GetType(String))
        table.Columns.Add("Quantity", GetType(String))
        table.Columns.Add("UnitPrice", GetType(String))

        DataGridView1.DataSource = table


    End Sub
    Private Sub clear()
        ItemIDComboBox.Text = "--Select Item ID--"
        NameTextBox.Clear()
        SizeTextBox.Clear()
        QuantityTextBox.Text = "0"
        UnitPriceTextBox.Clear()
        QuantityTextBox.Clear()
        ItemsSoldTextBox.Clear()
        TotalPriceTextBox.Clear()
        AmountGivenTextBox.Clear()
        AmountDueTextBox.Clear()
        table.Clear()

    End Sub


    Private Sub CalculateTotal()
        Dim sum As Integer = 0
        For i As Integer = 0 To DataGridView1.Rows.Count - 1
            sum += (Convert.ToInt32(DataGridView1.Rows(i).Cells(4).Value) * Convert.ToInt32(DataGridView1.Rows(i).Cells(3).Value))
        Next i
        TotalPriceTextBox.Text = sum.ToString()

    End Sub
    Private Sub loadItemID()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.Items"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read
                Dim sname = reader.GetString("ID")
                ItemIDComboBox.Items.Add(sname)


            End While
            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try

    End Sub
    Private Sub populateItemID()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.Items where ID = '" & ItemIDComboBox.Text & "'"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read

                NameTextBox.Text = reader.GetString("Name")
                SizeTextBox.Text = reader.GetString("Size")
                QuantityItemTextBox.Text = reader.GetString("Quantity")
                UnitPriceTextBox.Text = reader.GetString("UnitPrice")


            End While
            Call closeconn()
            ' loadtable()



        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub

    Private Sub loadCustID()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.Customers"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read
                Dim sname = reader.GetString("ID")
                CusIDComboBox.Items.Add(sname)



            End While
            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try

    End Sub
    Private Sub populateCustID()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.Customers where ID = '" & CusIDComboBox.Text & "'"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read

                CustNameTextBox.Text = reader.GetString("CustName")
               


            End While
            Call closeconn()
            ' loadtable()



        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub updateItems()
        Try
            Dim j As Integer
            Dim rQty As Integer = (Val(QuantityItemTextBox.Text))
            Dim Qty As Integer = (Val(QuantityTextBox.Text))
            'If rQty < Qty Or rQty <= 0 Then
            '    MsgBox("Sorry! Insufficient Items", MsgBoxStyle.Critical)
            'Else
            j = rQty - Qty
            'End If

            'MsgBox(" j is " & j & "")
            Call openconn()
            Dim query As String
            query = "Update ntmsdata.Items set  Name='" & NameTextBox.Text & "', Size = '" & SizeTextBox.Text & "',   Quantity ='" & j & "',     UnitPrice ='" & UnitPriceTextBox.Text & "' where ID ='" & ItemIDComboBox.Text & "'"
            cmd = New MySqlCommand(query, con)

            cmd.Connection = con

            'If NameTextBox.Text = "" Then
            '    MsgBox("Please Enter Item Name", MsgBoxStyle.Critical, "warning")
            '    NameTextBox.Focus()
            'ElseIf SizeTextBox.Text = "" Then
            '    MsgBox("Please Enter Item Size", MsgBoxStyle.Critical, "warning")
            '    SizeTextBox.Focus()
            'ElseIf QuantityTextBox.Text = "" Then
            '    MsgBox("Please Enter Number of Items", MsgBoxStyle.Critical, "warning")
            '    QuantityTextBox.Focus()
            'ElseIf UnitPriceTextBox.Text = "" Then
            '    MsgBox("Please Enter Unit Price", MsgBoxStyle.Critical, "warning")
            '    UnitPriceTextBox.Focus()
            'Else
            If rQty < Qty Or rQty <= 0 Then
                MsgBox("Sorry! Insufficient Items", MsgBoxStyle.Critical)
                'ElseIf QuantityTextBox.Text = "" Then
                '    MsgBox("Please Enter Number of Items", MsgBoxStyle.Critical, "warning")
                '    QuantityTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()

                MsgBox("Added To Cart", MsgBoxStyle.Information)
                Call closeconn()
                'loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub soldItems()

        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.solditemslist (itemID, ItemName,Size , Quantity, UnitPrice, DateSold) Values (  '" & ItemIDComboBox.Text & "','" & NameTextBox.Text & "',  '" & SizeTextBox.Text & "',  '" & QuantityTextBox.Text & "',     '" & UnitPriceTextBox.Text & "','" & DateDepositedDateTimePicker.Text & "'  )"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If NameTextBox.Text = "" Then
                MsgBox("Please Enter Item Name", MsgBoxStyle.Critical, "warning")
                NameTextBox.Focus()
            ElseIf SizeTextBox.Text = "" Then
                MsgBox("Please Enter Item Size", MsgBoxStyle.Critical, "warning")
                SizeTextBox.Focus()
            ElseIf QuantityTextBox.Text = "" Then
                MsgBox("Please Enter Number of Items", MsgBoxStyle.Critical, "warning")
                QuantityTextBox.Focus()
            ElseIf UnitPriceTextBox.Text = "" Then
                MsgBox("Please Enter Unit Price", MsgBoxStyle.Critical, "warning")
                UnitPriceTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()
                MsgBox("Sale Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                'loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub SaveandPrint()
        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.oneoffsales (ItemsSold, TotalPrice,AmountGiven ,Balance,   DatePaid) Values (  '" & ItemsSoldTextBox.Text & "','" & TotalPriceTextBox.Text & "',  '" & AmountGivenTextBox.Text & "',  '" & AmountDueTextBox.Text & "',     '" & DateDepositedDateTimePicker.Text & "'  )"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If ItemsSoldTextBox.Text = "" Then
                MsgBox("Please Add Items to cart first", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            ElseIf TotalPriceTextBox.Text = "" Then
                MsgBox("Please Add Items to cart first", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            ElseIf (Val(AmountGivenTextBox.Text) < 0) Or (Val(AmountGivenTextBox.Text) < Val(TotalPriceTextBox.Text)) Then
                MsgBox("Insufficient Amount given. If customer doesn't have sufficient funds, Please contact admin for sale revertion", MsgBoxStyle.Critical, "warning")
                AmountGivenTextBox.Focus()
            ElseIf AmountDueTextBox.Text = "" Then
                MsgBox("Please Add Items to cart first", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            Else
                TotalPrice = Me.TotalPriceTextBox.Text
                AmountGiven = Me.AmountGivenTextBox.Text
                Balance = Me.AmountDueTextBox.Text
                cmd.ExecuteNonQuery()
                MsgBox("Printing...", MsgBoxStyle.Information)
                Call closeconn()
                'loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Public Sub printItemSale()
        Try
            If ItemsSoldTextBox.Text = "" Then
                MsgBox("Please Add Items to cart first", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            ElseIf TotalPriceTextBox.Text = "" Then
                MsgBox("Please Add Items to cart first", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            ElseIf (Val(AmountGivenTextBox.Text) < 0) Or (Val(AmountGivenTextBox.Text) < Val(TotalPriceTextBox.Text)) Then
                MsgBox("Insufficient Amount given. If customer doesn't have sufficient funds, Please contact admin for sale revertion", MsgBoxStyle.Critical, "warning")
                AmountGivenTextBox.Focus()
            ElseIf AmountDueTextBox.Text = "" Then
                MsgBox("Please Add Items to cart first", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            Else
                frmPrintItemSale.Show()

                frmPrintItemSale.TotalLabel.Text = TotalPrice
                frmPrintItemSale.AmountLabel.Text = AmountGiven
                frmPrintItemSale.ChangeLabel.Text = Balance

                Me.PrintForm1.DocumentName = "Nakuru Tiger Machinery Services"
                Me.PrintForm1.Print(frmPrintItemSale, PowerPacks.Printing.PrintForm.PrintOption.FullWindow)

                frmPrintItemSale.Close()
                NewItemButton.Enabled = True
                btnSave.Enabled = False
                btnAddToCart.Enabled = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub frmDepositItemSale_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        clear()
    End Sub

    Private Sub frmDepositItemSale_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        table.Columns.Clear()
        AddCart()
    End Sub

    Private Sub NewItemButton_Click(sender As Object, e As EventArgs) Handles NewItemButton.Click

    End Sub

    Private Sub btnAddToCart_Click(sender As Object, e As EventArgs) Handles btnAddToCart.Click

        Dim j As Integer
        Dim rQty As Integer = (Val(QuantityItemTextBox.Text))
        Dim Qty As Integer = (Val(QuantityTextBox.Text))

        If ItemIDComboBox.Text = "--Select Item ID--" Then
            MsgBox("Please Select or Enter Item ID", MsgBoxStyle.Critical, "warning")
            ItemIDComboBox.Focus()
        ElseIf NameTextBox.Text = "" Then
            MsgBox("Please Enter Item Name", MsgBoxStyle.Critical, "warning")
            NameTextBox.Focus()
        ElseIf SizeTextBox.Text = "" Then
            MsgBox("Please Enter Item Size", MsgBoxStyle.Critical, "warning")
            SizeTextBox.Focus()
        ElseIf QuantityTextBox.Text = "0" Then
            MsgBox("Please Enter Number of Items", MsgBoxStyle.Critical, "warning")
            QuantityTextBox.Focus()
        ElseIf QuantityTextBox.Text = "" Then
            MsgBox("Please Enter Number of Items", MsgBoxStyle.Critical, "warning")
            QuantityTextBox.Focus()
        ElseIf UnitPriceTextBox.Text = "" Then
            MsgBox("Please Enter Unit Price", MsgBoxStyle.Critical, "warning")
            UnitPriceTextBox.Focus()

        ElseIf rQty < Qty Or rQty <= 0 Then
            MsgBox("Sorry! Insufficient Items", MsgBoxStyle.Critical)
            QuantityTextBox.Clear()
            QuantityTextBox.Focus()
        Else
            NewItemButton.Enabled = False
            table.Rows.Add(ItemIDComboBox.Text.Trim(), NameTextBox.Text.Trim(), SizeTextBox.Text.Trim(), QuantityTextBox.Text.Trim(), UnitPriceTextBox.Text.Trim())
            frmPrintItemSale.Show()
            frmPrintItemSale.printTable.Rows.Add(NameTextBox.Text.Trim(), SizeTextBox.Text.Trim(), QuantityTextBox.Text.Trim(), UnitPriceTextBox.Text.Trim())
            frmPrintItemSale.Hide()
            j = table.Rows.Count
            ItemsSoldTextBox.Text = j

            updateItems()
            soldItems()

            loadItemID()
            populateItemID()

            CalculateTotal()
            'QuantityTextBox.Text = DataGridView1.CurrentRow.Cells("Quantity").Value.ToString()
            'For Each quantity In QuantityTextBox.Text

            '    'Dim row As DataGridViewRow

            '    quantity += QuantityTextBox.Text

            '    MsgBox("" & quantity & "")
            'Next

        End If

    End Sub

    Private Sub ItemIDComboBox_DropDown(sender As Object, e As EventArgs) Handles ItemIDComboBox.DropDown
        ItemIDComboBox.Items.Clear()
        loadItemID()
        populateItemID()
    End Sub

    Private Sub ItemIDComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ItemIDComboBox.SelectedIndexChanged

    End Sub

    Private Sub ItemIDComboBox_TextChanged(sender As Object, e As EventArgs) Handles ItemIDComboBox.TextChanged

        loadItemID()
        populateItemID()
    End Sub

    Private Sub CusIDComboBox_DropDown(sender As Object, e As EventArgs) Handles CusIDComboBox.DropDown
        CusIDComboBox.Items.Clear()
        loadCustID()
        populateCustID()
    End Sub

    Private Sub CusIDComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CusIDComboBox.SelectedIndexChanged

    End Sub

    Private Sub CusIDComboBox_TextChanged(sender As Object, e As EventArgs) Handles CusIDComboBox.TextChanged
        loadCustID()
        populateCustID()
    End Sub
End Class