﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQuotation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim IDLabel As System.Windows.Forms.Label
        Dim ItemIDLabel As System.Windows.Forms.Label
        Dim SizeLabel As System.Windows.Forms.Label
        Dim QuantityLabel As System.Windows.Forms.Label
        Dim UnitPriceLabel As System.Windows.Forms.Label
        Dim ItemNameLabel As System.Windows.Forms.Label
        Dim ItemsQuotedLabel As System.Windows.Forms.Label
        Dim TotalPriceLabel As System.Windows.Forms.Label
        Dim IDLabel1 As System.Windows.Forms.Label
        Dim DeliverlyTypeLabel As System.Windows.Forms.Label
        Dim PaymentModeLabel As System.Windows.Forms.Label
        Dim TransportTypeLabel As System.Windows.Forms.Label
        Dim QuoteExpiryDateLabel As System.Windows.Forms.Label
        Dim CustomerNameLabel As System.Windows.Forms.Label
        Dim CompanyLabel As System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.IDTextBox = New System.Windows.Forms.TextBox()
        Me.ItemIDTextBox = New System.Windows.Forms.TextBox()
        Me.SizeTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        Me.UnitPriceTextBox = New System.Windows.Forms.TextBox()
        Me.ItemNameComboBox = New System.Windows.Forms.ComboBox()
        Me.ItemsQuotedTextBox = New System.Windows.Forms.TextBox()
        Me.TotalPriceTextBox = New System.Windows.Forms.TextBox()
        Me.CustomerIDTextBox = New System.Windows.Forms.TextBox()
        Me.DeliverlyTypeComboBox = New System.Windows.Forms.ComboBox()
        Me.PaymentModeComboBox = New System.Windows.Forms.ComboBox()
        Me.TransportTypeComboBox = New System.Windows.Forms.ComboBox()
        Me.QuoteExpiryDateTextBox = New System.Windows.Forms.TextBox()
        Me.CustomerNameComboBox = New System.Windows.Forms.ComboBox()
        Me.CompanyTextBox = New System.Windows.Forms.TextBox()
        IDLabel = New System.Windows.Forms.Label()
        ItemIDLabel = New System.Windows.Forms.Label()
        SizeLabel = New System.Windows.Forms.Label()
        QuantityLabel = New System.Windows.Forms.Label()
        UnitPriceLabel = New System.Windows.Forms.Label()
        ItemNameLabel = New System.Windows.Forms.Label()
        ItemsQuotedLabel = New System.Windows.Forms.Label()
        TotalPriceLabel = New System.Windows.Forms.Label()
        IDLabel1 = New System.Windows.Forms.Label()
        DeliverlyTypeLabel = New System.Windows.Forms.Label()
        PaymentModeLabel = New System.Windows.Forms.Label()
        TransportTypeLabel = New System.Windows.Forms.Label()
        QuoteExpiryDateLabel = New System.Windows.Forms.Label()
        CustomerNameLabel = New System.Windows.Forms.Label()
        CompanyLabel = New System.Windows.Forms.Label()
        Me.Panel5.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.DataGridView1)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel5.Size = New System.Drawing.Size(571, 228)
        Me.Panel5.TabIndex = 2
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(10, 10)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(511, 208)
        Me.DataGridView1.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(ItemsQuotedLabel)
        Me.Panel4.Controls.Add(Me.TotalPriceTextBox)
        Me.Panel4.Controls.Add(TotalPriceLabel)
        Me.Panel4.Controls.Add(Me.ItemsQuotedTextBox)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 228)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel4.Size = New System.Drawing.Size(571, 228)
        Me.Panel4.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(323, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(571, 456)
        Me.Panel3.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(894, 456)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(IDLabel1)
        Me.Panel2.Controls.Add(Me.CustomerIDTextBox)
        Me.Panel2.Controls.Add(DeliverlyTypeLabel)
        Me.Panel2.Controls.Add(Me.DeliverlyTypeComboBox)
        Me.Panel2.Controls.Add(PaymentModeLabel)
        Me.Panel2.Controls.Add(Me.PaymentModeComboBox)
        Me.Panel2.Controls.Add(TransportTypeLabel)
        Me.Panel2.Controls.Add(Me.TransportTypeComboBox)
        Me.Panel2.Controls.Add(QuoteExpiryDateLabel)
        Me.Panel2.Controls.Add(Me.QuoteExpiryDateTextBox)
        Me.Panel2.Controls.Add(CustomerNameLabel)
        Me.Panel2.Controls.Add(Me.CustomerNameComboBox)
        Me.Panel2.Controls.Add(CompanyLabel)
        Me.Panel2.Controls.Add(Me.CompanyTextBox)
        Me.Panel2.Controls.Add(ItemNameLabel)
        Me.Panel2.Controls.Add(Me.ItemNameComboBox)
        Me.Panel2.Controls.Add(IDLabel)
        Me.Panel2.Controls.Add(Me.IDTextBox)
        Me.Panel2.Controls.Add(ItemIDLabel)
        Me.Panel2.Controls.Add(Me.ItemIDTextBox)
        Me.Panel2.Controls.Add(SizeLabel)
        Me.Panel2.Controls.Add(Me.SizeTextBox)
        Me.Panel2.Controls.Add(QuantityLabel)
        Me.Panel2.Controls.Add(Me.QuantityTextBox)
        Me.Panel2.Controls.Add(UnitPriceLabel)
        Me.Panel2.Controls.Add(Me.UnitPriceTextBox)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(100, 10, 10, 10)
        Me.Panel2.Size = New System.Drawing.Size(362, 456)
        Me.Panel2.TabIndex = 0
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(32, 16)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(21, 13)
        IDLabel.TabIndex = 0
        IDLabel.Text = "ID:"
        '
        'IDTextBox
        '
        Me.IDTextBox.Location = New System.Drawing.Point(134, 13)
        Me.IDTextBox.Name = "IDTextBox"
        Me.IDTextBox.Size = New System.Drawing.Size(100, 20)
        Me.IDTextBox.TabIndex = 1
        '
        'ItemIDLabel
        '
        ItemIDLabel.AutoSize = True
        ItemIDLabel.Location = New System.Drawing.Point(32, 42)
        ItemIDLabel.Name = "ItemIDLabel"
        ItemIDLabel.Size = New System.Drawing.Size(44, 13)
        ItemIDLabel.TabIndex = 2
        ItemIDLabel.Text = "Item ID:"
        '
        'ItemIDTextBox
        '
        Me.ItemIDTextBox.Location = New System.Drawing.Point(134, 39)
        Me.ItemIDTextBox.Name = "ItemIDTextBox"
        Me.ItemIDTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ItemIDTextBox.TabIndex = 3
        '
        'SizeLabel
        '
        SizeLabel.AutoSize = True
        SizeLabel.Location = New System.Drawing.Point(32, 94)
        SizeLabel.Name = "SizeLabel"
        SizeLabel.Size = New System.Drawing.Size(30, 13)
        SizeLabel.TabIndex = 6
        SizeLabel.Text = "Size:"
        '
        'SizeTextBox
        '
        Me.SizeTextBox.Location = New System.Drawing.Point(134, 91)
        Me.SizeTextBox.Name = "SizeTextBox"
        Me.SizeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SizeTextBox.TabIndex = 7
        '
        'QuantityLabel
        '
        QuantityLabel.AutoSize = True
        QuantityLabel.Location = New System.Drawing.Point(32, 120)
        QuantityLabel.Name = "QuantityLabel"
        QuantityLabel.Size = New System.Drawing.Size(49, 13)
        QuantityLabel.TabIndex = 8
        QuantityLabel.Text = "Quantity:"
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.Location = New System.Drawing.Point(134, 117)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(100, 20)
        Me.QuantityTextBox.TabIndex = 9
        '
        'UnitPriceLabel
        '
        UnitPriceLabel.AutoSize = True
        UnitPriceLabel.Location = New System.Drawing.Point(32, 146)
        UnitPriceLabel.Name = "UnitPriceLabel"
        UnitPriceLabel.Size = New System.Drawing.Size(56, 13)
        UnitPriceLabel.TabIndex = 10
        UnitPriceLabel.Text = "Unit Price:"
        '
        'UnitPriceTextBox
        '
        Me.UnitPriceTextBox.Location = New System.Drawing.Point(134, 143)
        Me.UnitPriceTextBox.Name = "UnitPriceTextBox"
        Me.UnitPriceTextBox.Size = New System.Drawing.Size(100, 20)
        Me.UnitPriceTextBox.TabIndex = 11
        '
        'ItemNameLabel
        '
        ItemNameLabel.AutoSize = True
        ItemNameLabel.Location = New System.Drawing.Point(32, 71)
        ItemNameLabel.Name = "ItemNameLabel"
        ItemNameLabel.Size = New System.Drawing.Size(61, 13)
        ItemNameLabel.TabIndex = 11
        ItemNameLabel.Text = "Item Name:"
        '
        'ItemNameComboBox
        '
        Me.ItemNameComboBox.FormattingEnabled = True
        Me.ItemNameComboBox.Location = New System.Drawing.Point(134, 63)
        Me.ItemNameComboBox.Name = "ItemNameComboBox"
        Me.ItemNameComboBox.Size = New System.Drawing.Size(121, 21)
        Me.ItemNameComboBox.TabIndex = 12
        '
        'ItemsQuotedLabel
        '
        ItemsQuotedLabel.AutoSize = True
        ItemsQuotedLabel.Location = New System.Drawing.Point(34, 32)
        ItemsQuotedLabel.Name = "ItemsQuotedLabel"
        ItemsQuotedLabel.Size = New System.Drawing.Size(73, 13)
        ItemsQuotedLabel.TabIndex = 14
        ItemsQuotedLabel.Text = "Items Quoted:"
        '
        'ItemsQuotedTextBox
        '
        Me.ItemsQuotedTextBox.Location = New System.Drawing.Point(136, 29)
        Me.ItemsQuotedTextBox.Name = "ItemsQuotedTextBox"
        Me.ItemsQuotedTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ItemsQuotedTextBox.TabIndex = 15
        '
        'TotalPriceLabel
        '
        TotalPriceLabel.AutoSize = True
        TotalPriceLabel.Location = New System.Drawing.Point(303, 35)
        TotalPriceLabel.Name = "TotalPriceLabel"
        TotalPriceLabel.Size = New System.Drawing.Size(61, 13)
        TotalPriceLabel.TabIndex = 16
        TotalPriceLabel.Text = "Total Price:"
        '
        'TotalPriceTextBox
        '
        Me.TotalPriceTextBox.Location = New System.Drawing.Point(405, 32)
        Me.TotalPriceTextBox.Name = "TotalPriceTextBox"
        Me.TotalPriceTextBox.Size = New System.Drawing.Size(100, 20)
        Me.TotalPriceTextBox.TabIndex = 17
        '
        'IDLabel1
        '
        IDLabel1.AutoSize = True
        IDLabel1.Location = New System.Drawing.Point(32, 284)
        IDLabel1.Name = "IDLabel1"
        IDLabel1.Size = New System.Drawing.Size(68, 13)
        IDLabel1.TabIndex = 12
        IDLabel1.Text = "Customer ID:"
        '
        'CustomerIDTextBox
        '
        Me.CustomerIDTextBox.Location = New System.Drawing.Point(134, 281)
        Me.CustomerIDTextBox.Name = "CustomerIDTextBox"
        Me.CustomerIDTextBox.Size = New System.Drawing.Size(121, 20)
        Me.CustomerIDTextBox.TabIndex = 13
        '
        'DeliverlyTypeLabel
        '
        DeliverlyTypeLabel.AutoSize = True
        DeliverlyTypeLabel.Location = New System.Drawing.Point(32, 172)
        DeliverlyTypeLabel.Name = "DeliverlyTypeLabel"
        DeliverlyTypeLabel.Size = New System.Drawing.Size(77, 13)
        DeliverlyTypeLabel.TabIndex = 18
        DeliverlyTypeLabel.Text = "Deliverly Type:"
        '
        'DeliverlyTypeComboBox
        '
        Me.DeliverlyTypeComboBox.FormattingEnabled = True
        Me.DeliverlyTypeComboBox.Location = New System.Drawing.Point(134, 169)
        Me.DeliverlyTypeComboBox.Name = "DeliverlyTypeComboBox"
        Me.DeliverlyTypeComboBox.Size = New System.Drawing.Size(121, 21)
        Me.DeliverlyTypeComboBox.TabIndex = 19
        '
        'PaymentModeLabel
        '
        PaymentModeLabel.AutoSize = True
        PaymentModeLabel.Location = New System.Drawing.Point(32, 199)
        PaymentModeLabel.Name = "PaymentModeLabel"
        PaymentModeLabel.Size = New System.Drawing.Size(81, 13)
        PaymentModeLabel.TabIndex = 20
        PaymentModeLabel.Text = "Payment Mode:"
        '
        'PaymentModeComboBox
        '
        Me.PaymentModeComboBox.FormattingEnabled = True
        Me.PaymentModeComboBox.Location = New System.Drawing.Point(134, 196)
        Me.PaymentModeComboBox.Name = "PaymentModeComboBox"
        Me.PaymentModeComboBox.Size = New System.Drawing.Size(121, 21)
        Me.PaymentModeComboBox.TabIndex = 21
        '
        'TransportTypeLabel
        '
        TransportTypeLabel.AutoSize = True
        TransportTypeLabel.Location = New System.Drawing.Point(32, 226)
        TransportTypeLabel.Name = "TransportTypeLabel"
        TransportTypeLabel.Size = New System.Drawing.Size(82, 13)
        TransportTypeLabel.TabIndex = 22
        TransportTypeLabel.Text = "Transport Type:"
        '
        'TransportTypeComboBox
        '
        Me.TransportTypeComboBox.FormattingEnabled = True
        Me.TransportTypeComboBox.Location = New System.Drawing.Point(134, 223)
        Me.TransportTypeComboBox.Name = "TransportTypeComboBox"
        Me.TransportTypeComboBox.Size = New System.Drawing.Size(121, 21)
        Me.TransportTypeComboBox.TabIndex = 23
        '
        'QuoteExpiryDateLabel
        '
        QuoteExpiryDateLabel.AutoSize = True
        QuoteExpiryDateLabel.Location = New System.Drawing.Point(32, 253)
        QuoteExpiryDateLabel.Name = "QuoteExpiryDateLabel"
        QuoteExpiryDateLabel.Size = New System.Drawing.Size(96, 13)
        QuoteExpiryDateLabel.TabIndex = 24
        QuoteExpiryDateLabel.Text = "Quote Expiry Date:"
        '
        'QuoteExpiryDateTextBox
        '
        Me.QuoteExpiryDateTextBox.Location = New System.Drawing.Point(134, 250)
        Me.QuoteExpiryDateTextBox.Name = "QuoteExpiryDateTextBox"
        Me.QuoteExpiryDateTextBox.Size = New System.Drawing.Size(121, 20)
        Me.QuoteExpiryDateTextBox.TabIndex = 25
        '
        'CustomerNameLabel
        '
        CustomerNameLabel.AutoSize = True
        CustomerNameLabel.Location = New System.Drawing.Point(32, 310)
        CustomerNameLabel.Name = "CustomerNameLabel"
        CustomerNameLabel.Size = New System.Drawing.Size(85, 13)
        CustomerNameLabel.TabIndex = 26
        CustomerNameLabel.Text = "Customer Name:"
        '
        'CustomerNameComboBox
        '
        Me.CustomerNameComboBox.FormattingEnabled = True
        Me.CustomerNameComboBox.Location = New System.Drawing.Point(134, 307)
        Me.CustomerNameComboBox.Name = "CustomerNameComboBox"
        Me.CustomerNameComboBox.Size = New System.Drawing.Size(121, 21)
        Me.CustomerNameComboBox.TabIndex = 27
        '
        'CompanyLabel
        '
        CompanyLabel.AutoSize = True
        CompanyLabel.Location = New System.Drawing.Point(32, 337)
        CompanyLabel.Name = "CompanyLabel"
        CompanyLabel.Size = New System.Drawing.Size(54, 13)
        CompanyLabel.TabIndex = 28
        CompanyLabel.Text = "Company:"
        '
        'CompanyTextBox
        '
        Me.CompanyTextBox.Location = New System.Drawing.Point(134, 334)
        Me.CompanyTextBox.Name = "CompanyTextBox"
        Me.CompanyTextBox.Size = New System.Drawing.Size(121, 20)
        Me.CompanyTextBox.TabIndex = 29
        '
        'frmQuotation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 456)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmQuotation"
        Me.Text = "frmQuotation"
        Me.Panel5.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents IDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SizeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents QuantityTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UnitPriceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TotalPriceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemsQuotedTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CustomerIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DeliverlyTypeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PaymentModeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TransportTypeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents QuoteExpiryDateTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CustomerNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CompanyTextBox As System.Windows.Forms.TextBox
End Class
