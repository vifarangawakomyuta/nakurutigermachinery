﻿Imports MySql.Data.MySqlClient
Imports System.Text.RegularExpressions

Public Class frmEmployee
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader

    Private Sub loadtable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.employee"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub searchTable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.employee Where FirstName like '%" + txtSearch.Text + "%'"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Function EmailAddressCheck(ByVal emailAddress As String) As Boolean

        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        'Dim pattern As String = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
        Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
        If emailAddressMatch.Success Then
            EmailAddressCheck = True
        Else
            EmailAddressCheck = False
        End If

    End Function
    Private Sub clear()
        IDTextBox.Clear()
        FirstNameTextBox.Clear()
        LastNameTextBox.Clear()
        TelephoneTextBox.Clear()
        EmailTextBox.Clear()
    End Sub
    Private Sub saveItems()
        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.employee ( FirstName,LastName , Telephone, Email) Values (  '" & FirstNameTextBox.Text & "',  '" & LastNameTextBox.Text & "',  '" & TelephoneTextBox.Text & "',     '" & EmailTextBox.Text & "'  )"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If FirstNameTextBox.Text = "" Then
                MsgBox("Please Enter First Name", MsgBoxStyle.Critical, "warning")
                FirstNameTextBox.Focus()
            ElseIf LastNameTextBox.Text = "" Then
                MsgBox("Please Enter Last Name", MsgBoxStyle.Critical, "warning")
                LastNameTextBox.Focus()
            ElseIf TelephoneTextBox.Text = "" Then
                MsgBox("Please Enter Telephone Number", MsgBoxStyle.Critical, "warning")
                TelephoneTextBox.Focus()
            ElseIf EmailTextBox.Text = "" Then
                MsgBox("Please Enter Email", MsgBoxStyle.Critical, "warning")
                EmailTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()
                MsgBox("Save Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub updateItems()
        Try

            Call openconn()
            Dim query As String
            query = "Update ntmsdata.employee set  FirstName='" & FirstNameTextBox.Text & "', LastName = '" & LastNameTextBox.Text & "',   Telephone ='" & TelephoneTextBox.Text & "',     Email ='" & EmailTextBox.Text & "' where ID ='" & IDTextBox.Text & "'"
            cmd = New MySqlCommand(query, con)

            cmd.Connection = con

            If FirstNameTextBox.Text = "" Then
                MsgBox("Please Enter First Name", MsgBoxStyle.Critical, "warning")
                FirstNameTextBox.Focus()
            ElseIf LastNameTextBox.Text = "" Then
                MsgBox("Please Enter Last Name", MsgBoxStyle.Critical, "warning")
                LastNameTextBox.Focus()
            ElseIf TelephoneTextBox.Text = "" Then
                MsgBox("Please Enter Telephone Number", MsgBoxStyle.Critical, "warning")
                TelephoneTextBox.Focus()
            ElseIf EmailTextBox.Text = "" Then
                MsgBox("Please Enter Email", MsgBoxStyle.Critical, "warning")
                EmailTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()

                MsgBox("Update Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub deleteItems()
        Try
            If MessageBox.Show("Do you really want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then

                Call openconn()
                Dim query As String
                query = "delete from ntmsdata.employee where ID ='" & IDTextBox.Text & "'  "
                cmd = New MySqlCommand(query, con)
                reader = cmd.ExecuteReader
                MsgBox("Delete Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub



    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Call clear()
    End Sub

    Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub frmEmployee_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        emailVal.Text = ""
        loadtable()
    End Sub

    Private Sub EmailTextBox_TextChanged(sender As Object, e As EventArgs) Handles EmailTextBox.TextChanged
        EmailTextBox.BackColor = Color.White
        Dim temp As String
        temp = EmailTextBox.Text
        'Dim conditon As Boolean = False
        If EmailAddressCheck(temp) = True Then
            ': If emailaddresscheck(conditon) = True Then
            EmailTextBox.BackColor = Color.LightGreen
            emailVal.Text = "Valid Email"
        Else
            'MessageBox.Show("Please enter your email address correctly", "Incorrect Email Entry")
            'TextBox1.Text = ""
            EmailTextBox.BackColor = Color.Red
            emailVal.Text = "invalid Email"
        End If
    End Sub

    Private Sub TelephoneTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TelephoneTextBox.KeyPress
        Dim NotAllowed As String = "0123456789"
        If e.KeyChar <> ControlChars.Back Then
            If NotAllowed.IndexOf(e.KeyChar) = -1 Then
                e.Handled = True
                MsgBox("Letters not allowed...", MsgBoxStyle.Critical, "Attention....")
            End If
        End If
    End Sub

    Private Sub TelephoneTextBox_MaskInputRejected(sender As Object, e As MaskInputRejectedEventArgs) Handles TelephoneTextBox.MaskInputRejected

    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        loadtable()
        searchTable()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Call saveItems()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Call updateItems()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Call deleteItems()
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If e.RowIndex >= 0 Then
            'gets a collection that contains all the rows
            Dim row As DataGridViewRow = Me.DataGridView1.Rows(e.RowIndex)
            'populate the textbox from specific value of the coordinates of column and row.


            IDTextBox.Text = row.Cells(0).Value.ToString()
            FirstNameTextBox.Text = row.Cells(1).Value.ToString()
            LastNameTextBox.Text = row.Cells(2).Value.ToString()
            TelephoneTextBox.Text = row.Cells(3).Value.ToString()
            EmailTextBox.Text = row.Cells(4).Value.ToString()


            If IDTextBox.Text = "" Then
                MsgBox("Please Select A row with data on the datagrid View below", MsgBoxStyle.Critical)

            End If

        End If
    End Sub
End Class