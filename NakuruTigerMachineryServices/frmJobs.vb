﻿Imports MySql.Data.MySqlClient
Public Class frmJobs
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Private Sub loadJobstable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.jobs"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub searchTable()
        '    Try
        '        Call openconn()
        '        Dim query As String
        '        Dim ada As New MySqlDataAdapter
        '        Dim dalpset As New DataTable
        '        Dim dalpsource As New BindingSource
        '        'query = "select * from ntmsdata.Items Where Name like '%" + txtSearch.Text + "%'"
        '        cmd = New MySqlCommand(query, con)
        '        ada.SelectCommand = cmd
        '        ada.Fill(dalpset)
        '        dalpsource.DataSource = dalpset
        '        DataGridView1.DataSource = dalpsource
        '        ada.Update(dalpset)

        '        Call closeconn()

        '    Catch ex As MySqlException
        '        MessageBox.Show(ex.Message)
        '        con.Dispose()

        '    End Try
    End Sub
    Private Sub loadEmployeeID()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.employee"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read
                Dim sname = reader.GetString("ID")
                EmpIDComboBox.Items.Add(sname)


            End While
            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try

    End Sub
    Private Sub loadJobName()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.jobNames"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read
                Dim sname = reader.GetString("IdJobNames")
                JobNameIDComboBox.Items.Add(sname)


            End While
            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try

    End Sub
    Private Sub loadItemName()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.Items"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read
                Dim sname = reader.GetString("ID")
                ItemIDComboBox.Items.Add(sname)


            End While
            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try

    End Sub
    Private Sub populateEmployeeID()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.employee where ID = '" & EmpIDComboBox.Text & "'"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read

                EmpNameTextBox.Text = reader.GetString("FirstName")


            End While
            Call closeconn()
            loadJobstable()



        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub populateItemName()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.JobNames where IdJobNames = '" & ItemIDComboBox.Text & "'"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read

                NameTextBox.Text = reader.GetString("Name")
                SizeTextBox.Text = reader.GetString("Description")


            End While
            Call closeconn()
            ' loadtable()



        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub populateJobName()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.JobNames where idJobNames = '" & JobNameIDComboBox.Text & "'"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read

                JobNameTextBox.Text = reader.GetString("Name")
                JobDescriptionTextBox.Text = reader.GetString("Description")


            End While
            Call closeconn()
            ' loadtable()



        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub PopulateUnitPrice()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.Items where ID  = '" & ItemIDComboBox.Text & "'  and quantity > 0 "
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read

                'quantityItemTextBox.Text = reader.GetString("Quantity")
                UnitPriceTextBox.Text = reader.GetString("UnitPrice")

            End While
            Call closeconn()
            ' loadtable()



        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub

    Private Sub clearFinishedProduct()
        NameTextBox.Clear()
        ItemIDComboBox.Text = "--Select Item Name--"
        SizeTextBox.Clear()
        QuantityTextBox.Clear()
        UnitPriceTextBox.Clear()
    End Sub
    Private Sub clearItemInvention()
        IDTextBox.Clear()
        JobNameTextBox.Clear()
        JobNameIDComboBox.Text = "--Select Job Name--"
        EmpIDComboBox.Text = "--Select Employee ID--"
        EmpNameTextBox.Clear()
    End Sub

    Private Sub saveJob()
        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.jobs ( JobNameID,JobName,JobDescription,EmpID , EmpName, StartDate) Values (  '" & JobNameIDComboBox.Text & "', '" & JobNameTextBox.Text & "', '" & JobDescriptionTextBox.Text & "','" & EmpIDComboBox.Text & "',  '" & EmpNameTextBox.Text & "',     '" & StartDateDateTimePicker.Text & "'  )"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If JobNameIDComboBox.Text = "--Select Job Name ID--" Then
                MsgBox("Please Enter Job Name", MsgBoxStyle.Critical, "warning")
                JobNameIDComboBox.Focus()
            ElseIf JobDescriptionTextBox.Text = "" Then
                MsgBox("Please Select Job Name", MsgBoxStyle.Critical, "warning")
                JobDescriptionTextBox.Focus()
            ElseIf JobNameTextBox.Text = "" Then
                MsgBox("Please Select Job Name", MsgBoxStyle.Critical, "warning")
                JobNameIDComboBox.Focus()
            ElseIf JobNameTextBox.Text = "" Then
                MsgBox("Please Select Employee ID", MsgBoxStyle.Critical, "warning")
                EmpIDComboBox.Focus()
            ElseIf EmpNameTextBox.Text = "" Then
                MsgBox("Please Enter Employee Name", MsgBoxStyle.Critical, "warning")
                EmpNameTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()
                MsgBox("Job addition Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadJobstable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub getQuantity()

        Call openconn()
        Dim i As Integer

        Dim cmd As New MySqlCommand("SELECT Quantity FROM ntmsdata.Items  where ID ='" & ItemIDComboBox.Text & "' and  Quantity > 0", con)
        i = cmd.ExecuteScalar()
        cmd = Nothing
        Call closeconn()
        quantityItemTextBox.Text = i
    End Sub
    Private Sub updateItems()
        Try
            Dim j As Integer
            j = (Val(QuantityTextBox.Text)) + (Val(quantityItemTextBox.Text))
            'MsgBox("The value of j is " & j & "")
            Call openconn()
            Dim query As String
            query = "Update ntmsdata.Items set  Name='" & NameTextBox.Text & "', Size = '" & SizeTextBox.Text & "',   Quantity = " & j & ",     UnitPrice ='" & UnitPriceTextBox.Text & "' where ID ='" & ItemIDComboBox.Text & "'"
            cmd = New MySqlCommand(query, con)

            cmd.Connection = con

            If ItemIDComboBox.Text = "--Select Item Name ID--" Then
                MsgBox("Please Enter Item Name", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            ElseIf NameTextBox.Text = "" Then
                MsgBox("Please Select Item Name", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            ElseIf SizeTextBox.Text = "" Then
                MsgBox("Please Enter Item Size", MsgBoxStyle.Critical, "warning")
                SizeTextBox.Focus()
            ElseIf QuantityTextBox.Text = "" Then
                MsgBox("Please Enter Number of Items", MsgBoxStyle.Critical, "warning")
                QuantityTextBox.Focus()
            ElseIf UnitPriceTextBox.Text = "" Then
                MsgBox("Please Enter Unit Price", MsgBoxStyle.Critical, "warning")
                UnitPriceTextBox.Focus()
            ElseIf InventionIDTextBox.Text = "" Then
                MsgBox("Please Enter the ID of the Finished Item", MsgBoxStyle.Critical, "warning")
                InventionIDTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()

                MsgBox("Update Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadJobstable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub saveInventItems()
        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.Items ( ID, Name, Size) Values (  '" & JobNameIDComboBox.Text & "', '" & JobNameTextBox.Text & "', '" & JobDescriptionTextBox.Text & "')"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If JobNameIDComboBox.Text = "--Select Job Name ID--" Then
                MsgBox("Please Enter Job Name", MsgBoxStyle.Critical, "warning")
                JobNameIDComboBox.Focus()
            ElseIf JobNameTextBox.Text = "" Then
                MsgBox("Please Select Job Name", MsgBoxStyle.Critical, "warning")
                JobNameIDComboBox.Focus()
            Else
                cmd.ExecuteNonQuery()
                MsgBox("Item Invention Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                ' loadtable()
            End If
        Catch ex As MySqlException



            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub updateFinish()
        Try

            Call openconn()
            Dim query As String
            query = "Update ntmsdata.jobs set  EndDate='" & EndDateDateTimePicker.Text & "', Wage='" & WageTextBox.Text & "' where ID ='" & InventionIDTextBox.Text & "'"
            cmd = New MySqlCommand(query, con)

            cmd.Connection = con

            If ItemIDComboBox.Text = "--Select Item Name--" Then
                MsgBox("Please Enter Item Name", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            ElseIf NameTextBox.Text = "" Then
                MsgBox("Please Select Item Name", MsgBoxStyle.Critical, "warning")
                ItemIDComboBox.Focus()
            ElseIf SizeTextBox.Text = "" Then
                MsgBox("Please Enter Item Size", MsgBoxStyle.Critical, "warning")
                SizeTextBox.Focus()
            ElseIf QuantityTextBox.Text = "" Then
                MsgBox("Please Enter Number of Items", MsgBoxStyle.Critical, "warning")
                QuantityTextBox.Focus()
            ElseIf UnitPriceTextBox.Text = "" Then
                MsgBox("Please Enter Unit Price", MsgBoxStyle.Critical, "warning")
                UnitPriceTextBox.Focus()
            ElseIf InventionIDTextBox.Text = "" Then
                MsgBox("Please Enter the ID of the Finished Item", MsgBoxStyle.Critical, "warning")
                InventionIDTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()

                MsgBox("Job Finished Sucessful", MsgBoxStyle.Information)
                Call closeconn()

                loadItemName()
                loadJobstable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        'If e.RowIndex >= 0 Then
        '    'gets a collection that contains all the rows
        '    Dim row As DataGridViewRow = Me.DataGridView1.Rows(e.RowIndex)
        '    'populate the textbox from specific value of the coordinates of column and row.


        '    IDTextBox.Text = row.Cells(0).Value.ToString()
        '    JobNameIDTextBox.Text = row.Cells(1).Value.ToString()
        '    JobNameComboBox.Text = row.Cells(2).Value.ToString()
        '    EmpIDComboBox.Text = row.Cells(3).Value.ToString()
        '    ' EmpNameTextBox.Text = row.Cells(4).Value.ToString()
        '    'StartDateDateTimePicker.Text = row.Cells(4).Value.ToString()
        '    'EndDateDateTimePicker.Text = row.Cells(5).Value.ToString()
        '    WageTextBox.Text = row.Cells(6).Value.ToString()


        '    If IDTextBox.Text = "" Then
        '        MsgBox("Please Select A row with data on the datagrid View below", MsgBoxStyle.Critical)

        '    End If

        'End If
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Try
        '    Call openconn()
        '    Dim query As String
        '    Dim ada As New MySqlDataAdapter
        '    Dim dalpset As New DataTable
        '    Dim dalpsource As New BindingSource
        '    query = "select * from ntmsdata.Items Where Name like '%" + JobNameComboBox.Text + "%'"
        '    cmd = New MySqlCommand(query, con)

        '    ada.SelectCommand = cmd
        '    ada.Fill(dalpset)
        '    If dalpset.Rows Is Nothing Then
        '        saveInventItems()
        '        saveJob()
        '        MsgBox("item and jobs")
        '    Else
        '        saveJob()
        '        MsgBox("job only")
        '    End If
        '    dalpsource.DataSource = dalpset
        '    DataGridView1.DataSource = dalpsource
        '    ada.Update(dalpset)



        '    Call closeconn()

        'Catch ex As MySqlException
        '    MessageBox.Show(ex.Message)
        '    con.Dispose()

        'End Try

        saveJob()
        saveInventItems()

        clearItemInvention()
        ItemIDComboBox.Items.Clear()
        loadItemName()
        JobNameIDComboBox.Items.Clear()
        loadJobName()
        EmpIDComboBox.Items.Clear()
        loadEmployeeID()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        updateFinish()
        updateItems()

        clearFinishedProduct()
        ItemIDComboBox.Items.Clear()
        loadItemName()
        JobNameIDComboBox.Items.Clear()
        loadJobName()
        EmpIDComboBox.Items.Clear()
        loadEmployeeID()
    End Sub

    Private Sub JobNameComboBox_DropDown(sender As Object, e As EventArgs) Handles JobNameIDComboBox.DropDown
        JobNameIDComboBox.Items.Clear()
        loadJobName()
        populateJobName()

    End Sub

    Private Sub JobNameComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles JobNameIDComboBox.SelectedIndexChanged

    End Sub

    Private Sub EmpIDComboBox_DropDown(sender As Object, e As EventArgs) Handles EmpIDComboBox.DropDown
        EmpIDComboBox.Items.Clear()
        loadEmployeeID()
        populateEmployeeID()
    End Sub

    Private Sub EmpIDComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles EmpIDComboBox.SelectedIndexChanged

    End Sub

    Private Sub NameComboBox_DropDown(sender As Object, e As EventArgs) Handles ItemIDComboBox.DropDown
        ItemIDComboBox.Items.Clear()
        loadItemName()
        populateItemName()
    End Sub

    Private Sub NameComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ItemIDComboBox.SelectedIndexChanged

    End Sub

    Private Sub frmJobs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadJobstable()

    End Sub

    Private Sub JobNameComboBox_TextChanged(sender As Object, e As EventArgs) Handles JobNameIDComboBox.TextChanged
        'JobNameComboBox.Items.Clear()
        loadJobName()
        populateJobName()
    End Sub

    Private Sub EmpIDComboBox_TextChanged(sender As Object, e As EventArgs) Handles EmpIDComboBox.TextChanged
        'EmpIDComboBox.Items.Clear()
        loadEmployeeID()
        populateEmployeeID()
    End Sub

    Private Sub NameComboBox_TextChanged(sender As Object, e As EventArgs) Handles ItemIDComboBox.TextChanged
        'NameComboBox.Items.Clear()

        loadItemName()
        populateItemName()
        getQuantity()
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)
        updateFinish()
        loadJobstable()
    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub ItemIDTextBox_TextChanged(sender As Object, e As EventArgs) Handles NameTextBox.TextChanged
        'PopulateUnitPrice()
    End Sub

    Private Sub SizeTextBox_TextChanged(sender As Object, e As EventArgs) Handles SizeTextBox.TextChanged

    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs)
        getQuantity()
    End Sub

    Private Sub UnitPriceTextBox_TextChanged(sender As Object, e As EventArgs) Handles UnitPriceTextBox.TextChanged
        Dim q As Double
        Dim p As Double
        Dim wage As Double

        q = (Val(QuantityTextBox.Text))
        p = (Val(UnitPriceTextBox.Text))

        wage = (10 / 100) * p * q

        WageTextBox.Text = wage
    End Sub

    Private Sub QuantityTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles QuantityTextBox.KeyPress
        Dim NotAllowed As String = "0123456789"
        If e.KeyChar <> ControlChars.Back Then
            If NotAllowed.IndexOf(e.KeyChar) = -1 Then
                e.Handled = True
                MsgBox("Letters not allowed...", MsgBoxStyle.Critical, "Attention....")
            End If
        End If
    End Sub

    Private Sub QuantityTextBox_TextChanged(sender As Object, e As EventArgs) Handles QuantityTextBox.TextChanged
        Dim q As Double
        Dim p As Double
        Dim wage As Double

        q = (Val(QuantityTextBox.Text))
        p = (Val(UnitPriceTextBox.Text))

        wage = (10 / 100) * p * q

        WageTextBox.Text = wage



    End Sub

    Private Sub WageTextBox_TextChanged(sender As Object, e As EventArgs) Handles WageTextBox.TextChanged

    End Sub

    Private Sub UnitPriceTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles UnitPriceTextBox.KeyPress
        Dim NotAllowed As String = "0123456789"
        If e.KeyChar <> ControlChars.Back Then
            If NotAllowed.IndexOf(e.KeyChar) = -1 Then
                e.Handled = True
                MsgBox("Letters not allowed...", MsgBoxStyle.Critical, "Attention....")
            End If
        End If
    End Sub

    Private Sub InventionIDTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles InventionIDTextBox.KeyPress
        Dim NotAllowed As String = "0123456789"
        If e.KeyChar <> ControlChars.Back Then
            If NotAllowed.IndexOf(e.KeyChar) = -1 Then
                e.Handled = True
                MsgBox("Letters not allowed...", MsgBoxStyle.Critical, "Attention....")
            End If
        End If
    End Sub
End Class