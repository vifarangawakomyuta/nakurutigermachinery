﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SellItemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SellItemComponentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MakeItemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SetItemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SetItemComponentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SetJobNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddUserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddSupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddEmployeeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepositSaleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.SettingsToolStripMenuItem, Me.AddToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(905, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SellItemsToolStripMenuItem, Me.SellItemComponentsToolStripMenuItem, Me.DepositSaleToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'SellItemsToolStripMenuItem
        '
        Me.SellItemsToolStripMenuItem.Name = "SellItemsToolStripMenuItem"
        Me.SellItemsToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.SellItemsToolStripMenuItem.Text = "sell Items"
        '
        'SellItemComponentsToolStripMenuItem
        '
        Me.SellItemComponentsToolStripMenuItem.Name = "SellItemComponentsToolStripMenuItem"
        Me.SellItemComponentsToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.SellItemComponentsToolStripMenuItem.Text = "Sell Item Components"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MakeItemsToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'MakeItemsToolStripMenuItem
        '
        Me.MakeItemsToolStripMenuItem.Name = "MakeItemsToolStripMenuItem"
        Me.MakeItemsToolStripMenuItem.Size = New System.Drawing.Size(135, 22)
        Me.MakeItemsToolStripMenuItem.Text = "Make Items"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SetItemsToolStripMenuItem, Me.SetItemComponentsToolStripMenuItem, Me.SetJobNamesToolStripMenuItem})
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'SetItemsToolStripMenuItem
        '
        Me.SetItemsToolStripMenuItem.Name = "SetItemsToolStripMenuItem"
        Me.SetItemsToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.SetItemsToolStripMenuItem.Text = "Set Items"
        '
        'SetItemComponentsToolStripMenuItem
        '
        Me.SetItemComponentsToolStripMenuItem.Name = "SetItemComponentsToolStripMenuItem"
        Me.SetItemComponentsToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.SetItemComponentsToolStripMenuItem.Text = "Set Item Components"
        '
        'SetJobNamesToolStripMenuItem
        '
        Me.SetJobNamesToolStripMenuItem.Name = "SetJobNamesToolStripMenuItem"
        Me.SetJobNamesToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.SetJobNamesToolStripMenuItem.Text = "Set Job Names"
        '
        'AddToolStripMenuItem
        '
        Me.AddToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddUserToolStripMenuItem, Me.AddSupplierToolStripMenuItem, Me.AddCustomerToolStripMenuItem, Me.AddEmployeeToolStripMenuItem})
        Me.AddToolStripMenuItem.Name = "AddToolStripMenuItem"
        Me.AddToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.AddToolStripMenuItem.Text = "Add"
        '
        'AddUserToolStripMenuItem
        '
        Me.AddUserToolStripMenuItem.Name = "AddUserToolStripMenuItem"
        Me.AddUserToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AddUserToolStripMenuItem.Text = "Add User"
        '
        'AddSupplierToolStripMenuItem
        '
        Me.AddSupplierToolStripMenuItem.Name = "AddSupplierToolStripMenuItem"
        Me.AddSupplierToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AddSupplierToolStripMenuItem.Text = "Add Supplier"
        '
        'AddCustomerToolStripMenuItem
        '
        Me.AddCustomerToolStripMenuItem.Name = "AddCustomerToolStripMenuItem"
        Me.AddCustomerToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AddCustomerToolStripMenuItem.Text = "Add Customer"
        '
        'AddEmployeeToolStripMenuItem
        '
        Me.AddEmployeeToolStripMenuItem.Name = "AddEmployeeToolStripMenuItem"
        Me.AddEmployeeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AddEmployeeToolStripMenuItem.Text = "Add Employee"
        '
        'DepositSaleToolStripMenuItem
        '
        Me.DepositSaleToolStripMenuItem.Name = "DepositSaleToolStripMenuItem"
        Me.DepositSaleToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.DepositSaleToolStripMenuItem.Text = "Deposit Sale"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(905, 460)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SellItemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SellItemComponentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MakeItemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SetItemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SetItemComponentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddUserToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddSupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SetJobNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddEmployeeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepositSaleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
