﻿Public Class Form1

    Private Sub SellItemsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SellItemsToolStripMenuItem.Click
        frmItemSale.ShowDialog()

    End Sub

    Private Sub SellItemComponentsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SellItemComponentsToolStripMenuItem.Click
        frmItemComponentSale.ShowDialog()
    End Sub

    Private Sub MakeItemsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MakeItemsToolStripMenuItem.Click
        frmJobs.Show()
    End Sub

    Private Sub SetItemsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SetItemsToolStripMenuItem.Click
        frmItems.Show()
    End Sub

    Private Sub SetItemComponentsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SetItemComponentsToolStripMenuItem.Click
        frmItemComponents.Show()
    End Sub

    Private Sub SetJobNamesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SetJobNamesToolStripMenuItem.Click
        frmJobNames.Show()
    End Sub

    Private Sub AddUserToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddUserToolStripMenuItem.Click
        frmUsers.Show()
    End Sub

    Private Sub AddSupplierToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddSupplierToolStripMenuItem.Click
        frmsupplier.Show()
    End Sub

    Private Sub AddCustomerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddCustomerToolStripMenuItem.Click
        frmCustomer.Show()
    End Sub

    Private Sub AddEmployeeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddEmployeeToolStripMenuItem.Click
        frmEmployee.Show()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub DepositSaleToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DepositSaleToolStripMenuItem.Click
        frmDepositItemSale.ShowDialog()
    End Sub
End Class
