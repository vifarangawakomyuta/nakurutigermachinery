﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBorrowTools
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ToolIDLabel As System.Windows.Forms.Label
        Dim QuantityLabel As System.Windows.Forms.Label
        Dim EmpIDLabel As System.Windows.Forms.Label
        Dim EmpNameLabel As System.Windows.Forms.Label
        Dim IDLabel As System.Windows.Forms.Label
        Dim TotalToolsLabel As System.Windows.Forms.Label
        Dim ToolNameLabel As System.Windows.Forms.Label
        Dim DateBorrowedLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim ToolValueLabel As System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.TotalToolsTextBox = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DateBorrowedDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ToolNameComboBox = New System.Windows.Forms.ComboBox()
        Me.ToolBorrowedIDTextBox = New System.Windows.Forms.TextBox()
        Me.ToolIDTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        Me.EmpIDTextBox = New System.Windows.Forms.TextBox()
        Me.EmpNameTextBox = New System.Windows.Forms.TextBox()
        Me.UnitValueTextBox = New System.Windows.Forms.TextBox()
        Me.ToolValueTextBox = New System.Windows.Forms.TextBox()
        ToolIDLabel = New System.Windows.Forms.Label()
        QuantityLabel = New System.Windows.Forms.Label()
        EmpIDLabel = New System.Windows.Forms.Label()
        EmpNameLabel = New System.Windows.Forms.Label()
        IDLabel = New System.Windows.Forms.Label()
        TotalToolsLabel = New System.Windows.Forms.Label()
        ToolNameLabel = New System.Windows.Forms.Label()
        DateBorrowedLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        ToolValueLabel = New System.Windows.Forms.Label()
        Me.Panel5.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolIDLabel
        '
        ToolIDLabel.AutoSize = True
        ToolIDLabel.Location = New System.Drawing.Point(16, 67)
        ToolIDLabel.Name = "ToolIDLabel"
        ToolIDLabel.Size = New System.Drawing.Size(45, 13)
        ToolIDLabel.TabIndex = 2
        ToolIDLabel.Text = "Tool ID:"
        '
        'QuantityLabel
        '
        QuantityLabel.AutoSize = True
        QuantityLabel.Location = New System.Drawing.Point(16, 119)
        QuantityLabel.Name = "QuantityLabel"
        QuantityLabel.Size = New System.Drawing.Size(49, 13)
        QuantityLabel.TabIndex = 6
        QuantityLabel.Text = "Quantity:"
        '
        'EmpIDLabel
        '
        EmpIDLabel.AutoSize = True
        EmpIDLabel.Location = New System.Drawing.Point(16, 201)
        EmpIDLabel.Name = "EmpIDLabel"
        EmpIDLabel.Size = New System.Drawing.Size(45, 13)
        EmpIDLabel.TabIndex = 10
        EmpIDLabel.Text = "Emp ID:"
        '
        'EmpNameLabel
        '
        EmpNameLabel.AutoSize = True
        EmpNameLabel.Location = New System.Drawing.Point(16, 227)
        EmpNameLabel.Name = "EmpNameLabel"
        EmpNameLabel.Size = New System.Drawing.Size(62, 13)
        EmpNameLabel.TabIndex = 12
        EmpNameLabel.Text = "Emp Name:"
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(16, 41)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(90, 13)
        IDLabel.TabIndex = 15
        IDLabel.Text = "Tool BorrowedID:"
        '
        'TotalToolsLabel
        '
        TotalToolsLabel.AutoSize = True
        TotalToolsLabel.Location = New System.Drawing.Point(311, 29)
        TotalToolsLabel.Name = "TotalToolsLabel"
        TotalToolsLabel.Size = New System.Drawing.Size(63, 13)
        TotalToolsLabel.TabIndex = 19
        TotalToolsLabel.Text = "Total Tools:"
        '
        'ToolNameLabel
        '
        ToolNameLabel.AutoSize = True
        ToolNameLabel.Location = New System.Drawing.Point(16, 99)
        ToolNameLabel.Name = "ToolNameLabel"
        ToolNameLabel.Size = New System.Drawing.Size(62, 13)
        ToolNameLabel.TabIndex = 16
        ToolNameLabel.Text = "Tool Name:"
        '
        'DateBorrowedLabel
        '
        DateBorrowedLabel.AutoSize = True
        DateBorrowedLabel.Location = New System.Drawing.Point(16, 250)
        DateBorrowedLabel.Name = "DateBorrowedLabel"
        DateBorrowedLabel.Size = New System.Drawing.Size(81, 13)
        DateBorrowedLabel.TabIndex = 17
        DateBorrowedLabel.Text = "Date Borrowed:"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.DataGridView1)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel5.Size = New System.Drawing.Size(571, 228)
        Me.Panel5.TabIndex = 2
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(10, 10)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(511, 208)
        Me.DataGridView1.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.TotalToolsTextBox)
        Me.Panel4.Controls.Add(TotalToolsLabel)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 228)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel4.Size = New System.Drawing.Size(571, 228)
        Me.Panel4.TabIndex = 1
        '
        'TotalToolsTextBox
        '
        Me.TotalToolsTextBox.Location = New System.Drawing.Point(407, 26)
        Me.TotalToolsTextBox.Name = "TotalToolsTextBox"
        Me.TotalToolsTextBox.Size = New System.Drawing.Size(100, 20)
        Me.TotalToolsTextBox.TabIndex = 20
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(323, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(571, 456)
        Me.Panel3.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(894, 456)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.UnitValueTextBox)
        Me.Panel2.Controls.Add(Label1)
        Me.Panel2.Controls.Add(ToolValueLabel)
        Me.Panel2.Controls.Add(Me.ToolValueTextBox)
        Me.Panel2.Controls.Add(DateBorrowedLabel)
        Me.Panel2.Controls.Add(Me.DateBorrowedDateTimePicker)
        Me.Panel2.Controls.Add(ToolNameLabel)
        Me.Panel2.Controls.Add(Me.ToolNameComboBox)
        Me.Panel2.Controls.Add(IDLabel)
        Me.Panel2.Controls.Add(Me.ToolBorrowedIDTextBox)
        Me.Panel2.Controls.Add(ToolIDLabel)
        Me.Panel2.Controls.Add(Me.ToolIDTextBox)
        Me.Panel2.Controls.Add(QuantityLabel)
        Me.Panel2.Controls.Add(Me.QuantityTextBox)
        Me.Panel2.Controls.Add(EmpIDLabel)
        Me.Panel2.Controls.Add(Me.EmpIDTextBox)
        Me.Panel2.Controls.Add(EmpNameLabel)
        Me.Panel2.Controls.Add(Me.EmpNameTextBox)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(100, 10, 10, 10)
        Me.Panel2.Size = New System.Drawing.Size(327, 456)
        Me.Panel2.TabIndex = 0
        '
        'DateBorrowedDateTimePicker
        '
        Me.DateBorrowedDateTimePicker.Location = New System.Drawing.Point(112, 250)
        Me.DateBorrowedDateTimePicker.Name = "DateBorrowedDateTimePicker"
        Me.DateBorrowedDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.DateBorrowedDateTimePicker.TabIndex = 18
        '
        'ToolNameComboBox
        '
        Me.ToolNameComboBox.FormattingEnabled = True
        Me.ToolNameComboBox.Location = New System.Drawing.Point(112, 89)
        Me.ToolNameComboBox.Name = "ToolNameComboBox"
        Me.ToolNameComboBox.Size = New System.Drawing.Size(200, 21)
        Me.ToolNameComboBox.TabIndex = 17
        '
        'ToolBorrowedIDTextBox
        '
        Me.ToolBorrowedIDTextBox.Location = New System.Drawing.Point(112, 38)
        Me.ToolBorrowedIDTextBox.Name = "ToolBorrowedIDTextBox"
        Me.ToolBorrowedIDTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ToolBorrowedIDTextBox.TabIndex = 16
        '
        'ToolIDTextBox
        '
        Me.ToolIDTextBox.Location = New System.Drawing.Point(112, 64)
        Me.ToolIDTextBox.Name = "ToolIDTextBox"
        Me.ToolIDTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ToolIDTextBox.TabIndex = 3
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.Location = New System.Drawing.Point(112, 116)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(200, 20)
        Me.QuantityTextBox.TabIndex = 7
        '
        'EmpIDTextBox
        '
        Me.EmpIDTextBox.Location = New System.Drawing.Point(112, 198)
        Me.EmpIDTextBox.Name = "EmpIDTextBox"
        Me.EmpIDTextBox.Size = New System.Drawing.Size(200, 20)
        Me.EmpIDTextBox.TabIndex = 11
        '
        'EmpNameTextBox
        '
        Me.EmpNameTextBox.Location = New System.Drawing.Point(112, 224)
        Me.EmpNameTextBox.Name = "EmpNameTextBox"
        Me.EmpNameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.EmpNameTextBox.TabIndex = 13
        '
        'UnitValueTextBox
        '
        Me.UnitValueTextBox.Location = New System.Drawing.Point(112, 142)
        Me.UnitValueTextBox.Name = "UnitValueTextBox"
        Me.UnitValueTextBox.Size = New System.Drawing.Size(200, 20)
        Me.UnitValueTextBox.TabIndex = 22
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(18, 147)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(59, 13)
        Label1.TabIndex = 19
        Label1.Text = "Unit Value:"
        '
        'ToolValueLabel
        '
        ToolValueLabel.AutoSize = True
        ToolValueLabel.Location = New System.Drawing.Point(19, 175)
        ToolValueLabel.Name = "ToolValueLabel"
        ToolValueLabel.Size = New System.Drawing.Size(64, 13)
        ToolValueLabel.TabIndex = 20
        ToolValueLabel.Text = "Total Value:"
        '
        'ToolValueTextBox
        '
        Me.ToolValueTextBox.Location = New System.Drawing.Point(112, 170)
        Me.ToolValueTextBox.Name = "ToolValueTextBox"
        Me.ToolValueTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ToolValueTextBox.TabIndex = 21
        '
        'frmBorrowTools
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 456)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmBorrowTools"
        Me.Text = "frmBorrowTools"
        Me.Panel5.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents ToolIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents QuantityTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmpIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmpNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TotalToolsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolBorrowedIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DateBorrowedDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents UnitValueTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolValueTextBox As System.Windows.Forms.TextBox
End Class
