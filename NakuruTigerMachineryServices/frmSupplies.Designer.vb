﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSupplies
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim IDLabel As System.Windows.Forms.Label
        Dim ItemIDLabel As System.Windows.Forms.Label
        Dim ItemNameLabel As System.Windows.Forms.Label
        Dim QuantityLabel As System.Windows.Forms.Label
        Dim UnitPriceLabel As System.Windows.Forms.Label
        Dim TotalPriceLabel As System.Windows.Forms.Label
        Dim SupIDLabel As System.Windows.Forms.Label
        Dim SupNameLabel As System.Windows.Forms.Label
        Dim CompanyLabel As System.Windows.Forms.Label
        Dim DateSuppliedLabel As System.Windows.Forms.Label
        Dim InvoiceNoLabel As System.Windows.Forms.Label
        Dim AmountGivenLabel As System.Windows.Forms.Label
        Dim BalanceLabel As System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.InvoiceNoComboBox = New System.Windows.Forms.ComboBox()
        Me.BalanceTextBox = New System.Windows.Forms.TextBox()
        Me.AmountGivenTextBox = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.IDTextBox = New System.Windows.Forms.TextBox()
        Me.ItemIDTextBox = New System.Windows.Forms.TextBox()
        Me.ItemNameComboBox = New System.Windows.Forms.ComboBox()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        Me.UnitPriceTextBox = New System.Windows.Forms.TextBox()
        Me.TotalPriceTextBox = New System.Windows.Forms.TextBox()
        Me.SupIDTextBox = New System.Windows.Forms.TextBox()
        Me.SupNameComboBox = New System.Windows.Forms.ComboBox()
        Me.CompanyTextBox = New System.Windows.Forms.TextBox()
        Me.DateSuppliedDateTimePicker = New System.Windows.Forms.DateTimePicker()
        IDLabel = New System.Windows.Forms.Label()
        ItemIDLabel = New System.Windows.Forms.Label()
        ItemNameLabel = New System.Windows.Forms.Label()
        QuantityLabel = New System.Windows.Forms.Label()
        UnitPriceLabel = New System.Windows.Forms.Label()
        TotalPriceLabel = New System.Windows.Forms.Label()
        SupIDLabel = New System.Windows.Forms.Label()
        SupNameLabel = New System.Windows.Forms.Label()
        CompanyLabel = New System.Windows.Forms.Label()
        DateSuppliedLabel = New System.Windows.Forms.Label()
        InvoiceNoLabel = New System.Windows.Forms.Label()
        AmountGivenLabel = New System.Windows.Forms.Label()
        BalanceLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(22, 32)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(21, 13)
        IDLabel.TabIndex = 0
        IDLabel.Text = "ID:"
        '
        'ItemIDLabel
        '
        ItemIDLabel.AutoSize = True
        ItemIDLabel.Location = New System.Drawing.Point(22, 58)
        ItemIDLabel.Name = "ItemIDLabel"
        ItemIDLabel.Size = New System.Drawing.Size(44, 13)
        ItemIDLabel.TabIndex = 2
        ItemIDLabel.Text = "Item ID:"
        '
        'ItemNameLabel
        '
        ItemNameLabel.AutoSize = True
        ItemNameLabel.Location = New System.Drawing.Point(22, 84)
        ItemNameLabel.Name = "ItemNameLabel"
        ItemNameLabel.Size = New System.Drawing.Size(61, 13)
        ItemNameLabel.TabIndex = 4
        ItemNameLabel.Text = "Item Name:"
        '
        'QuantityLabel
        '
        QuantityLabel.AutoSize = True
        QuantityLabel.Location = New System.Drawing.Point(14, 104)
        QuantityLabel.Name = "QuantityLabel"
        QuantityLabel.Padding = New System.Windows.Forms.Padding(10)
        QuantityLabel.Size = New System.Drawing.Size(69, 33)
        QuantityLabel.TabIndex = 6
        QuantityLabel.Text = "Quantity:"
        '
        'UnitPriceLabel
        '
        UnitPriceLabel.AutoSize = True
        UnitPriceLabel.Location = New System.Drawing.Point(22, 137)
        UnitPriceLabel.Name = "UnitPriceLabel"
        UnitPriceLabel.Size = New System.Drawing.Size(56, 13)
        UnitPriceLabel.TabIndex = 8
        UnitPriceLabel.Text = "Unit Price:"
        '
        'TotalPriceLabel
        '
        TotalPriceLabel.AutoSize = True
        TotalPriceLabel.Location = New System.Drawing.Point(22, 163)
        TotalPriceLabel.Name = "TotalPriceLabel"
        TotalPriceLabel.Size = New System.Drawing.Size(61, 13)
        TotalPriceLabel.TabIndex = 10
        TotalPriceLabel.Text = "Total Price:"
        '
        'SupIDLabel
        '
        SupIDLabel.AutoSize = True
        SupIDLabel.Location = New System.Drawing.Point(22, 189)
        SupIDLabel.Name = "SupIDLabel"
        SupIDLabel.Size = New System.Drawing.Size(43, 13)
        SupIDLabel.TabIndex = 12
        SupIDLabel.Text = "Sup ID:"
        '
        'SupNameLabel
        '
        SupNameLabel.AutoSize = True
        SupNameLabel.Location = New System.Drawing.Point(22, 215)
        SupNameLabel.Name = "SupNameLabel"
        SupNameLabel.Size = New System.Drawing.Size(60, 13)
        SupNameLabel.TabIndex = 14
        SupNameLabel.Text = "Sup Name:"
        '
        'CompanyLabel
        '
        CompanyLabel.AutoSize = True
        CompanyLabel.Location = New System.Drawing.Point(22, 242)
        CompanyLabel.Name = "CompanyLabel"
        CompanyLabel.Size = New System.Drawing.Size(54, 13)
        CompanyLabel.TabIndex = 16
        CompanyLabel.Text = "Company:"
        '
        'DateSuppliedLabel
        '
        DateSuppliedLabel.AutoSize = True
        DateSuppliedLabel.Location = New System.Drawing.Point(22, 269)
        DateSuppliedLabel.Name = "DateSuppliedLabel"
        DateSuppliedLabel.Size = New System.Drawing.Size(77, 13)
        DateSuppliedLabel.TabIndex = 18
        DateSuppliedLabel.Text = "Date Supplied:"
        '
        'InvoiceNoLabel
        '
        InvoiceNoLabel.AutoSize = True
        InvoiceNoLabel.Location = New System.Drawing.Point(13, 19)
        InvoiceNoLabel.Name = "InvoiceNoLabel"
        InvoiceNoLabel.Size = New System.Drawing.Size(62, 13)
        InvoiceNoLabel.TabIndex = 20
        InvoiceNoLabel.Text = "Invoice No:"
        '
        'AmountGivenLabel
        '
        AmountGivenLabel.AutoSize = True
        AmountGivenLabel.Location = New System.Drawing.Point(13, 46)
        AmountGivenLabel.Name = "AmountGivenLabel"
        AmountGivenLabel.Size = New System.Drawing.Size(77, 13)
        AmountGivenLabel.TabIndex = 22
        AmountGivenLabel.Text = "Amount Given:"
        '
        'BalanceLabel
        '
        BalanceLabel.AutoSize = True
        BalanceLabel.Location = New System.Drawing.Point(13, 72)
        BalanceLabel.Name = "BalanceLabel"
        BalanceLabel.Size = New System.Drawing.Size(49, 13)
        BalanceLabel.TabIndex = 24
        BalanceLabel.Text = "Balance:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(936, 604)
        Me.Panel1.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(311, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(625, 604)
        Me.Panel3.TabIndex = 1
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.InvoiceNoComboBox)
        Me.Panel5.Controls.Add(Me.BalanceTextBox)
        Me.Panel5.Controls.Add(BalanceLabel)
        Me.Panel5.Controls.Add(Me.AmountGivenTextBox)
        Me.Panel5.Controls.Add(AmountGivenLabel)
        Me.Panel5.Controls.Add(InvoiceNoLabel)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 354)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(625, 250)
        Me.Panel5.TabIndex = 1
        '
        'InvoiceNoComboBox
        '
        Me.InvoiceNoComboBox.FormattingEnabled = True
        Me.InvoiceNoComboBox.Location = New System.Drawing.Point(96, 16)
        Me.InvoiceNoComboBox.Name = "InvoiceNoComboBox"
        Me.InvoiceNoComboBox.Size = New System.Drawing.Size(200, 21)
        Me.InvoiceNoComboBox.TabIndex = 21
        '
        'BalanceTextBox
        '
        Me.BalanceTextBox.Location = New System.Drawing.Point(96, 69)
        Me.BalanceTextBox.Name = "BalanceTextBox"
        Me.BalanceTextBox.Size = New System.Drawing.Size(200, 20)
        Me.BalanceTextBox.TabIndex = 25
        '
        'AmountGivenTextBox
        '
        Me.AmountGivenTextBox.Location = New System.Drawing.Point(96, 43)
        Me.AmountGivenTextBox.Name = "AmountGivenTextBox"
        Me.AmountGivenTextBox.Size = New System.Drawing.Size(200, 20)
        Me.AmountGivenTextBox.TabIndex = 23
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.DataGridView1)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel4.Size = New System.Drawing.Size(625, 348)
        Me.Panel4.TabIndex = 0
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(10, 10)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(565, 328)
        Me.DataGridView1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(IDLabel)
        Me.Panel2.Controls.Add(Me.IDTextBox)
        Me.Panel2.Controls.Add(ItemIDLabel)
        Me.Panel2.Controls.Add(Me.ItemIDTextBox)
        Me.Panel2.Controls.Add(ItemNameLabel)
        Me.Panel2.Controls.Add(Me.ItemNameComboBox)
        Me.Panel2.Controls.Add(QuantityLabel)
        Me.Panel2.Controls.Add(Me.QuantityTextBox)
        Me.Panel2.Controls.Add(UnitPriceLabel)
        Me.Panel2.Controls.Add(Me.UnitPriceTextBox)
        Me.Panel2.Controls.Add(TotalPriceLabel)
        Me.Panel2.Controls.Add(Me.TotalPriceTextBox)
        Me.Panel2.Controls.Add(SupIDLabel)
        Me.Panel2.Controls.Add(Me.SupIDTextBox)
        Me.Panel2.Controls.Add(SupNameLabel)
        Me.Panel2.Controls.Add(Me.SupNameComboBox)
        Me.Panel2.Controls.Add(CompanyLabel)
        Me.Panel2.Controls.Add(Me.CompanyTextBox)
        Me.Panel2.Controls.Add(DateSuppliedLabel)
        Me.Panel2.Controls.Add(Me.DateSuppliedDateTimePicker)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(425, 604)
        Me.Panel2.TabIndex = 0
        '
        'IDTextBox
        '
        Me.IDTextBox.Location = New System.Drawing.Point(105, 29)
        Me.IDTextBox.Name = "IDTextBox"
        Me.IDTextBox.Size = New System.Drawing.Size(200, 20)
        Me.IDTextBox.TabIndex = 1
        '
        'ItemIDTextBox
        '
        Me.ItemIDTextBox.Location = New System.Drawing.Point(105, 55)
        Me.ItemIDTextBox.Name = "ItemIDTextBox"
        Me.ItemIDTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ItemIDTextBox.TabIndex = 3
        '
        'ItemNameComboBox
        '
        Me.ItemNameComboBox.FormattingEnabled = True
        Me.ItemNameComboBox.Location = New System.Drawing.Point(105, 81)
        Me.ItemNameComboBox.Name = "ItemNameComboBox"
        Me.ItemNameComboBox.Size = New System.Drawing.Size(200, 21)
        Me.ItemNameComboBox.TabIndex = 5
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.Location = New System.Drawing.Point(105, 108)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(200, 20)
        Me.QuantityTextBox.TabIndex = 7
        '
        'UnitPriceTextBox
        '
        Me.UnitPriceTextBox.Location = New System.Drawing.Point(105, 134)
        Me.UnitPriceTextBox.Name = "UnitPriceTextBox"
        Me.UnitPriceTextBox.Size = New System.Drawing.Size(200, 20)
        Me.UnitPriceTextBox.TabIndex = 9
        '
        'TotalPriceTextBox
        '
        Me.TotalPriceTextBox.Location = New System.Drawing.Point(105, 160)
        Me.TotalPriceTextBox.Name = "TotalPriceTextBox"
        Me.TotalPriceTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TotalPriceTextBox.TabIndex = 11
        '
        'SupIDTextBox
        '
        Me.SupIDTextBox.Location = New System.Drawing.Point(105, 186)
        Me.SupIDTextBox.Name = "SupIDTextBox"
        Me.SupIDTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SupIDTextBox.TabIndex = 13
        '
        'SupNameComboBox
        '
        Me.SupNameComboBox.FormattingEnabled = True
        Me.SupNameComboBox.Location = New System.Drawing.Point(105, 212)
        Me.SupNameComboBox.Name = "SupNameComboBox"
        Me.SupNameComboBox.Size = New System.Drawing.Size(200, 21)
        Me.SupNameComboBox.TabIndex = 15
        '
        'CompanyTextBox
        '
        Me.CompanyTextBox.Location = New System.Drawing.Point(105, 239)
        Me.CompanyTextBox.Name = "CompanyTextBox"
        Me.CompanyTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CompanyTextBox.TabIndex = 17
        '
        'DateSuppliedDateTimePicker
        '
        Me.DateSuppliedDateTimePicker.Location = New System.Drawing.Point(105, 265)
        Me.DateSuppliedDateTimePicker.Name = "DateSuppliedDateTimePicker"
        Me.DateSuppliedDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.DateSuppliedDateTimePicker.TabIndex = 19
        '
        'frmSupplies
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(936, 604)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmSupplies"
        Me.Text = "frmSupplies"
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents InvoiceNoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents BalanceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AmountGivenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents IDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents QuantityTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UnitPriceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TotalPriceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SupIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SupNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CompanyTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DateSuppliedDateTimePicker As System.Windows.Forms.DateTimePicker
End Class
