﻿Imports MySql.Data.MySqlClient
Public Class frmItemComponents
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader

     Private Sub loadtable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.Itemcomponents"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub searchTable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.Itemcomponents Where Name like '%" + txtSearch.Text + "%'"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub

    Private Sub clear()
        IDTextBox.Clear()
        NameTextBox.Clear()
        SizeTextBox.Clear()
        QuantityTextBox.Clear()
        UnitPriceTextBox.Clear()
    End Sub
    Private Sub saveItems()
        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.Itemcomponents ( Name, Size , Quantity, UnitPrice) Values (  '" & NameTextBox.Text & "',  '" & SizeTextBox.Text & "',  '" & QuantityTextBox.Text & "',     '" & UnitPriceTextBox.Text & "'  )"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If NameTextBox.Text = "" Then
                MsgBox("Please Enter Item Name", MsgBoxStyle.Critical, "warning")
                NameTextBox.Focus()
            ElseIf SizeTextBox.Text = "" Then
                MsgBox("Please Enter Item Size", MsgBoxStyle.Critical, "warning")
                SizeTextBox.Focus()
            ElseIf QuantityTextBox.Text = "" Then
                MsgBox("Please Enter Number of Items", MsgBoxStyle.Critical, "warning")
                QuantityTextBox.Focus()
            ElseIf UnitPriceTextBox.Text = "" Then
                MsgBox("Please Enter Unit Price", MsgBoxStyle.Critical, "warning")
                UnitPriceTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()
                MsgBox("Save Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub updateItems()
        Try

            Call openconn()
            Dim query As String
            query = "Update ntmsdata.Itemcomponents set  Name='" & NameTextBox.Text & "', Size = '" & SizeTextBox.Text & "',   Quantity ='" & QuantityTextBox.Text & "',     UnitPrice ='" & UnitPriceTextBox.Text & "' where ID ='" & IDTextBox.Text & "'"
            cmd = New MySqlCommand(query, con)

            cmd.Connection = con
            If IDTextBox.Text = "" Then
                MsgBox("Please Select Item to be Updated in the table Below", MsgBoxStyle.Critical, "warning")
                IDTextBox.Focus()
            ElseIf NameTextBox.Text = "" Then
                MsgBox("Please Enter Item Name", MsgBoxStyle.Critical, "warning")
                NameTextBox.Focus()
            ElseIf SizeTextBox.Text = "" Then
                MsgBox("Please Enter Item Size", MsgBoxStyle.Critical, "warning")
                SizeTextBox.Focus()
            ElseIf QuantityTextBox.Text = "" Then
                MsgBox("Please Enter Number of Items", MsgBoxStyle.Critical, "warning")
                QuantityTextBox.Focus()
            ElseIf UnitPriceTextBox.Text = "" Then
                MsgBox("Please Enter Unit Price", MsgBoxStyle.Critical, "warning")
                UnitPriceTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()

                MsgBox("Update Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub deleteItems()
        Try
            If MessageBox.Show("Do you really want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then

                Call openconn()
                Dim query As String
                query = "delete from ntmsdata.Itemcomponents where ID ='" & IDTextBox.Text & "'  "
                cmd = New MySqlCommand(query, con)
                If IDTextBox.Text = "" Then
                    MsgBox("Please Select Item to be Deleted in the table Below", MsgBoxStyle.Critical, "warning")
                    IDTextBox.Focus()
                Else
                    reader = cmd.ExecuteReader
                    MsgBox("Delete Sucessful", MsgBoxStyle.Information)
                    Call closeconn()
                    loadtable()
                End If
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If e.RowIndex >= 0 Then
            'gets a collection that contains all the rows
            Dim row As DataGridViewRow = Me.DataGridView1.Rows(e.RowIndex)
            'populate the textbox from specific value of the coordinates of column and row.


            IDTextBox.Text = row.Cells(0).Value.ToString()
            NameTextBox.Text = row.Cells(1).Value.ToString()
            SizeTextBox.Text = row.Cells(2).Value.ToString()
            QuantityTextBox.Text = row.Cells(3).Value.ToString()
            UnitPriceTextBox.Text = row.Cells(4).Value.ToString()

            If IDTextBox.Text = "" Then
                MsgBox("Please Select A row with data on the datagrid View below", MsgBoxStyle.Critical)

            End If

        End If
    End Sub

    Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub QuantityTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles QuantityTextBox.KeyPress
        Dim NotAllowed As String = "0123456789"
        If e.KeyChar <> ControlChars.Back Then
            If NotAllowed.IndexOf(e.KeyChar) = -1 Then
                e.Handled = True
                MsgBox("Letters not allowed...", MsgBoxStyle.Critical, "Attention....")
            End If
        End If
    End Sub

    Private Sub QuantityTextBox_TextChanged(sender As Object, e As EventArgs) Handles QuantityTextBox.TextChanged

    End Sub

    Private Sub UnitPriceTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles UnitPriceTextBox.KeyPress
        Dim NotAllowed As String = "0123456789"
        If e.KeyChar <> ControlChars.Back Then
            If NotAllowed.IndexOf(e.KeyChar) = -1 Then
                e.Handled = True
                MsgBox("Letters not allowed...", MsgBoxStyle.Critical, "Attention....")
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Call clear()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Call saveItems()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Call updateItems()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Call deleteItems()
    End Sub

    Private Sub frmItemComponents_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadtable()
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        loadtable()
        searchTable()
    End Sub
End Class