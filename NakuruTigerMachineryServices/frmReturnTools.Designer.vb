﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReturnTools
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim IDLabel As System.Windows.Forms.Label
        Dim BorrowIDLabel As System.Windows.Forms.Label
        Dim TotalToolsLabel As System.Windows.Forms.Label
        Dim DateBorrowedLabel As System.Windows.Forms.Label
        Dim DateReturnedLabel As System.Windows.Forms.Label
        Dim EmpIDLabel As System.Windows.Forms.Label
        Dim EmpNameLabel As System.Windows.Forms.Label
        Dim MissingToolsLabel As System.Windows.Forms.Label
        Dim ToolIDLabel As System.Windows.Forms.Label
        Dim ToolNameLabel As System.Windows.Forms.Label
        Dim QuantityLabel As System.Windows.Forms.Label
        Dim DateReturnedLabel1 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim ToolValueLabel As System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.ToolsReturnedTextBox = New System.Windows.Forms.TextBox()
        Me.TotalToolsTextBox = New System.Windows.Forms.TextBox()
        Me.MissingToolsTextBox = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.ToolIDTextBox = New System.Windows.Forms.TextBox()
        Me.ToolNameTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        Me.DateReturnedTextBox1 = New System.Windows.Forms.TextBox()
        Me.ReturnToolsIDTextBox = New System.Windows.Forms.TextBox()
        Me.BorrowIDComboBox = New System.Windows.Forms.ComboBox()
        Me.DateBorrowedTextBox = New System.Windows.Forms.TextBox()
        Me.DateReturnedTextBox = New System.Windows.Forms.TextBox()
        Me.EmpIDTextBox = New System.Windows.Forms.TextBox()
        Me.EmpNameTextBox = New System.Windows.Forms.TextBox()
        Me.UnitValueTextBox = New System.Windows.Forms.TextBox()
        Me.ToolValueTextBox = New System.Windows.Forms.TextBox()
        IDLabel = New System.Windows.Forms.Label()
        BorrowIDLabel = New System.Windows.Forms.Label()
        TotalToolsLabel = New System.Windows.Forms.Label()
        DateBorrowedLabel = New System.Windows.Forms.Label()
        DateReturnedLabel = New System.Windows.Forms.Label()
        EmpIDLabel = New System.Windows.Forms.Label()
        EmpNameLabel = New System.Windows.Forms.Label()
        MissingToolsLabel = New System.Windows.Forms.Label()
        ToolIDLabel = New System.Windows.Forms.Label()
        ToolNameLabel = New System.Windows.Forms.Label()
        QuantityLabel = New System.Windows.Forms.Label()
        DateReturnedLabel1 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        ToolValueLabel = New System.Windows.Forms.Label()
        Me.Panel5.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(24, 39)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(85, 13)
        IDLabel.TabIndex = 0
        IDLabel.Text = "Return Tools ID:"
        '
        'BorrowIDLabel
        '
        BorrowIDLabel.AutoSize = True
        BorrowIDLabel.Location = New System.Drawing.Point(24, 65)
        BorrowIDLabel.Name = "BorrowIDLabel"
        BorrowIDLabel.Size = New System.Drawing.Size(57, 13)
        BorrowIDLabel.TabIndex = 2
        BorrowIDLabel.Text = "Borrow ID:"
        '
        'TotalToolsLabel
        '
        TotalToolsLabel.AutoSize = True
        TotalToolsLabel.Location = New System.Drawing.Point(15, 51)
        TotalToolsLabel.Name = "TotalToolsLabel"
        TotalToolsLabel.Size = New System.Drawing.Size(63, 13)
        TotalToolsLabel.TabIndex = 4
        TotalToolsLabel.Text = "Total Tools:"
        '
        'DateBorrowedLabel
        '
        DateBorrowedLabel.AutoSize = True
        DateBorrowedLabel.Location = New System.Drawing.Point(24, 236)
        DateBorrowedLabel.Name = "DateBorrowedLabel"
        DateBorrowedLabel.Size = New System.Drawing.Size(81, 13)
        DateBorrowedLabel.TabIndex = 6
        DateBorrowedLabel.Text = "Date Borrowed:"
        '
        'DateReturnedLabel
        '
        DateReturnedLabel.AutoSize = True
        DateReturnedLabel.Location = New System.Drawing.Point(24, 262)
        DateReturnedLabel.Name = "DateReturnedLabel"
        DateReturnedLabel.Size = New System.Drawing.Size(80, 13)
        DateReturnedLabel.TabIndex = 8
        DateReturnedLabel.Text = "Date Returned:"
        '
        'EmpIDLabel
        '
        EmpIDLabel.AutoSize = True
        EmpIDLabel.Location = New System.Drawing.Point(24, 288)
        EmpIDLabel.Name = "EmpIDLabel"
        EmpIDLabel.Size = New System.Drawing.Size(45, 13)
        EmpIDLabel.TabIndex = 10
        EmpIDLabel.Text = "Emp ID:"
        '
        'EmpNameLabel
        '
        EmpNameLabel.AutoSize = True
        EmpNameLabel.Location = New System.Drawing.Point(24, 314)
        EmpNameLabel.Name = "EmpNameLabel"
        EmpNameLabel.Size = New System.Drawing.Size(62, 13)
        EmpNameLabel.TabIndex = 12
        EmpNameLabel.Text = "Emp Name:"
        '
        'MissingToolsLabel
        '
        MissingToolsLabel.AutoSize = True
        MissingToolsLabel.Location = New System.Drawing.Point(261, 92)
        MissingToolsLabel.Name = "MissingToolsLabel"
        MissingToolsLabel.Size = New System.Drawing.Size(74, 13)
        MissingToolsLabel.TabIndex = 14
        MissingToolsLabel.Text = "Missing Tools:"
        '
        'ToolIDLabel
        '
        ToolIDLabel.AutoSize = True
        ToolIDLabel.Location = New System.Drawing.Point(25, 92)
        ToolIDLabel.Name = "ToolIDLabel"
        ToolIDLabel.Size = New System.Drawing.Size(45, 13)
        ToolIDLabel.TabIndex = 18
        ToolIDLabel.Text = "Tool ID:"
        '
        'ToolNameLabel
        '
        ToolNameLabel.AutoSize = True
        ToolNameLabel.Location = New System.Drawing.Point(25, 118)
        ToolNameLabel.Name = "ToolNameLabel"
        ToolNameLabel.Size = New System.Drawing.Size(62, 13)
        ToolNameLabel.TabIndex = 20
        ToolNameLabel.Text = "Tool Name:"
        '
        'QuantityLabel
        '
        QuantityLabel.AutoSize = True
        QuantityLabel.Location = New System.Drawing.Point(25, 211)
        QuantityLabel.Name = "QuantityLabel"
        QuantityLabel.Size = New System.Drawing.Size(49, 13)
        QuantityLabel.TabIndex = 24
        QuantityLabel.Text = "Quantity:"
        '
        'DateReturnedLabel1
        '
        DateReturnedLabel1.AutoSize = True
        DateReturnedLabel1.Location = New System.Drawing.Point(78, 473)
        DateReturnedLabel1.Name = "DateReturnedLabel1"
        DateReturnedLabel1.Size = New System.Drawing.Size(80, 13)
        DateReturnedLabel1.TabIndex = 30
        DateReturnedLabel1.Text = "Date Returned:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(261, 47)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(83, 13)
        Label1.TabIndex = 4
        Label1.Text = "Tools Returned:"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.DataGridView1)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel5.Size = New System.Drawing.Size(571, 228)
        Me.Panel5.TabIndex = 2
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(10, 10)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(511, 208)
        Me.DataGridView1.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.ToolsReturnedTextBox)
        Me.Panel4.Controls.Add(Label1)
        Me.Panel4.Controls.Add(Me.TotalToolsTextBox)
        Me.Panel4.Controls.Add(TotalToolsLabel)
        Me.Panel4.Controls.Add(Me.MissingToolsTextBox)
        Me.Panel4.Controls.Add(MissingToolsLabel)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 228)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel4.Size = New System.Drawing.Size(571, 228)
        Me.Panel4.TabIndex = 1
        '
        'ToolsReturnedTextBox
        '
        Me.ToolsReturnedTextBox.Location = New System.Drawing.Point(348, 44)
        Me.ToolsReturnedTextBox.Name = "ToolsReturnedTextBox"
        Me.ToolsReturnedTextBox.Size = New System.Drawing.Size(121, 20)
        Me.ToolsReturnedTextBox.TabIndex = 5
        '
        'TotalToolsTextBox
        '
        Me.TotalToolsTextBox.Location = New System.Drawing.Point(102, 48)
        Me.TotalToolsTextBox.Name = "TotalToolsTextBox"
        Me.TotalToolsTextBox.Size = New System.Drawing.Size(121, 20)
        Me.TotalToolsTextBox.TabIndex = 5
        '
        'MissingToolsTextBox
        '
        Me.MissingToolsTextBox.Location = New System.Drawing.Point(348, 89)
        Me.MissingToolsTextBox.Name = "MissingToolsTextBox"
        Me.MissingToolsTextBox.Size = New System.Drawing.Size(121, 20)
        Me.MissingToolsTextBox.TabIndex = 15
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(323, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(571, 456)
        Me.Panel3.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(894, 456)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.UnitValueTextBox)
        Me.Panel2.Controls.Add(Label2)
        Me.Panel2.Controls.Add(ToolValueLabel)
        Me.Panel2.Controls.Add(Me.ToolValueTextBox)
        Me.Panel2.Controls.Add(ToolIDLabel)
        Me.Panel2.Controls.Add(Me.ToolIDTextBox)
        Me.Panel2.Controls.Add(ToolNameLabel)
        Me.Panel2.Controls.Add(Me.ToolNameTextBox)
        Me.Panel2.Controls.Add(QuantityLabel)
        Me.Panel2.Controls.Add(Me.QuantityTextBox)
        Me.Panel2.Controls.Add(DateReturnedLabel1)
        Me.Panel2.Controls.Add(Me.DateReturnedTextBox1)
        Me.Panel2.Controls.Add(IDLabel)
        Me.Panel2.Controls.Add(Me.ReturnToolsIDTextBox)
        Me.Panel2.Controls.Add(BorrowIDLabel)
        Me.Panel2.Controls.Add(Me.BorrowIDComboBox)
        Me.Panel2.Controls.Add(DateBorrowedLabel)
        Me.Panel2.Controls.Add(Me.DateBorrowedTextBox)
        Me.Panel2.Controls.Add(DateReturnedLabel)
        Me.Panel2.Controls.Add(Me.DateReturnedTextBox)
        Me.Panel2.Controls.Add(EmpIDLabel)
        Me.Panel2.Controls.Add(Me.EmpIDTextBox)
        Me.Panel2.Controls.Add(EmpNameLabel)
        Me.Panel2.Controls.Add(Me.EmpNameTextBox)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(100, 10, 10, 10)
        Me.Panel2.Size = New System.Drawing.Size(344, 456)
        Me.Panel2.TabIndex = 0
        '
        'ToolIDTextBox
        '
        Me.ToolIDTextBox.Location = New System.Drawing.Point(111, 89)
        Me.ToolIDTextBox.Name = "ToolIDTextBox"
        Me.ToolIDTextBox.Size = New System.Drawing.Size(172, 20)
        Me.ToolIDTextBox.TabIndex = 19
        '
        'ToolNameTextBox
        '
        Me.ToolNameTextBox.Location = New System.Drawing.Point(111, 115)
        Me.ToolNameTextBox.Name = "ToolNameTextBox"
        Me.ToolNameTextBox.Size = New System.Drawing.Size(172, 20)
        Me.ToolNameTextBox.TabIndex = 21
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.Location = New System.Drawing.Point(111, 208)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(172, 20)
        Me.QuantityTextBox.TabIndex = 25
        '
        'DateReturnedTextBox1
        '
        Me.DateReturnedTextBox1.Location = New System.Drawing.Point(164, 470)
        Me.DateReturnedTextBox1.Name = "DateReturnedTextBox1"
        Me.DateReturnedTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.DateReturnedTextBox1.TabIndex = 31
        '
        'ReturnToolsIDTextBox
        '
        Me.ReturnToolsIDTextBox.Location = New System.Drawing.Point(111, 36)
        Me.ReturnToolsIDTextBox.Name = "ReturnToolsIDTextBox"
        Me.ReturnToolsIDTextBox.Size = New System.Drawing.Size(172, 20)
        Me.ReturnToolsIDTextBox.TabIndex = 1
        '
        'BorrowIDComboBox
        '
        Me.BorrowIDComboBox.FormattingEnabled = True
        Me.BorrowIDComboBox.Location = New System.Drawing.Point(111, 62)
        Me.BorrowIDComboBox.Name = "BorrowIDComboBox"
        Me.BorrowIDComboBox.Size = New System.Drawing.Size(172, 21)
        Me.BorrowIDComboBox.TabIndex = 3
        '
        'DateBorrowedTextBox
        '
        Me.DateBorrowedTextBox.Location = New System.Drawing.Point(111, 233)
        Me.DateBorrowedTextBox.Name = "DateBorrowedTextBox"
        Me.DateBorrowedTextBox.Size = New System.Drawing.Size(172, 20)
        Me.DateBorrowedTextBox.TabIndex = 7
        '
        'DateReturnedTextBox
        '
        Me.DateReturnedTextBox.Location = New System.Drawing.Point(111, 259)
        Me.DateReturnedTextBox.Name = "DateReturnedTextBox"
        Me.DateReturnedTextBox.Size = New System.Drawing.Size(172, 20)
        Me.DateReturnedTextBox.TabIndex = 9
        '
        'EmpIDTextBox
        '
        Me.EmpIDTextBox.Location = New System.Drawing.Point(111, 285)
        Me.EmpIDTextBox.Name = "EmpIDTextBox"
        Me.EmpIDTextBox.Size = New System.Drawing.Size(172, 20)
        Me.EmpIDTextBox.TabIndex = 11
        '
        'EmpNameTextBox
        '
        Me.EmpNameTextBox.Location = New System.Drawing.Point(111, 311)
        Me.EmpNameTextBox.Name = "EmpNameTextBox"
        Me.EmpNameTextBox.Size = New System.Drawing.Size(172, 20)
        Me.EmpNameTextBox.TabIndex = 13
        '
        'UnitValueTextBox
        '
        Me.UnitValueTextBox.Location = New System.Drawing.Point(111, 141)
        Me.UnitValueTextBox.Name = "UnitValueTextBox"
        Me.UnitValueTextBox.Size = New System.Drawing.Size(172, 20)
        Me.UnitValueTextBox.TabIndex = 35
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(24, 144)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(59, 13)
        Label2.TabIndex = 32
        Label2.Text = "Unit Value:"
        '
        'ToolValueLabel
        '
        ToolValueLabel.AutoSize = True
        ToolValueLabel.Location = New System.Drawing.Point(25, 172)
        ToolValueLabel.Name = "ToolValueLabel"
        ToolValueLabel.Size = New System.Drawing.Size(64, 13)
        ToolValueLabel.TabIndex = 33
        ToolValueLabel.Text = "Total Value:"
        '
        'ToolValueTextBox
        '
        Me.ToolValueTextBox.Location = New System.Drawing.Point(111, 169)
        Me.ToolValueTextBox.Name = "ToolValueTextBox"
        Me.ToolValueTextBox.Size = New System.Drawing.Size(172, 20)
        Me.ToolValueTextBox.TabIndex = 34
        '
        'frmReturnTools
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 456)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmReturnTools"
        Me.Text = "frmReturnTools"
        Me.Panel5.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents ReturnToolsIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BorrowIDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TotalToolsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DateBorrowedTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DateReturnedTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmpIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmpNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MissingToolsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolsReturnedTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents QuantityTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DateReturnedTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents UnitValueTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolValueTextBox As System.Windows.Forms.TextBox
End Class
