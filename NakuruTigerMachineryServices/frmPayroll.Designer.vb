﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayroll
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim IDLabel As System.Windows.Forms.Label
        Dim PayMonthLabel As System.Windows.Forms.Label
        Dim EmpIDLabel As System.Windows.Forms.Label
        Dim EmpNameLabel As System.Windows.Forms.Label
        Dim TotalJobsLabel As System.Windows.Forms.Label
        Dim TotalSalaryLabel As System.Windows.Forms.Label
        Dim AllowancesLabel As System.Windows.Forms.Label
        Dim MissingToolsChargesLabel As System.Windows.Forms.Label
        Dim DatePaidLabel As System.Windows.Forms.Label
        Me.IDTextBox = New System.Windows.Forms.TextBox()
        Me.PayMonthTextBox = New System.Windows.Forms.TextBox()
        Me.EmpIDTextBox = New System.Windows.Forms.TextBox()
        Me.EmpNameComboBox = New System.Windows.Forms.ComboBox()
        Me.TotalJobsTextBox = New System.Windows.Forms.TextBox()
        Me.TotalSalaryTextBox = New System.Windows.Forms.TextBox()
        Me.AllowancesTextBox = New System.Windows.Forms.TextBox()
        Me.MissingToolsChargesTextBox = New System.Windows.Forms.TextBox()
        Me.DatePaidTextBox = New System.Windows.Forms.TextBox()
        IDLabel = New System.Windows.Forms.Label()
        PayMonthLabel = New System.Windows.Forms.Label()
        EmpIDLabel = New System.Windows.Forms.Label()
        EmpNameLabel = New System.Windows.Forms.Label()
        TotalJobsLabel = New System.Windows.Forms.Label()
        TotalSalaryLabel = New System.Windows.Forms.Label()
        AllowancesLabel = New System.Windows.Forms.Label()
        MissingToolsChargesLabel = New System.Windows.Forms.Label()
        DatePaidLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(24, 37)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(21, 13)
        IDLabel.TabIndex = 1
        IDLabel.Text = "ID:"
        '
        'IDTextBox
        '
        Me.IDTextBox.Location = New System.Drawing.Point(146, 34)
        Me.IDTextBox.Name = "IDTextBox"
        Me.IDTextBox.Size = New System.Drawing.Size(121, 20)
        Me.IDTextBox.TabIndex = 2
        '
        'PayMonthLabel
        '
        PayMonthLabel.AutoSize = True
        PayMonthLabel.Location = New System.Drawing.Point(24, 63)
        PayMonthLabel.Name = "PayMonthLabel"
        PayMonthLabel.Size = New System.Drawing.Size(61, 13)
        PayMonthLabel.TabIndex = 3
        PayMonthLabel.Text = "Pay Month:"
        '
        'PayMonthTextBox
        '
        Me.PayMonthTextBox.Location = New System.Drawing.Point(146, 60)
        Me.PayMonthTextBox.Name = "PayMonthTextBox"
        Me.PayMonthTextBox.Size = New System.Drawing.Size(121, 20)
        Me.PayMonthTextBox.TabIndex = 4
        '
        'EmpIDLabel
        '
        EmpIDLabel.AutoSize = True
        EmpIDLabel.Location = New System.Drawing.Point(24, 89)
        EmpIDLabel.Name = "EmpIDLabel"
        EmpIDLabel.Size = New System.Drawing.Size(45, 13)
        EmpIDLabel.TabIndex = 5
        EmpIDLabel.Text = "Emp ID:"
        '
        'EmpIDTextBox
        '
        Me.EmpIDTextBox.Location = New System.Drawing.Point(146, 86)
        Me.EmpIDTextBox.Name = "EmpIDTextBox"
        Me.EmpIDTextBox.Size = New System.Drawing.Size(121, 20)
        Me.EmpIDTextBox.TabIndex = 6
        '
        'EmpNameLabel
        '
        EmpNameLabel.AutoSize = True
        EmpNameLabel.Location = New System.Drawing.Point(24, 115)
        EmpNameLabel.Name = "EmpNameLabel"
        EmpNameLabel.Size = New System.Drawing.Size(62, 13)
        EmpNameLabel.TabIndex = 7
        EmpNameLabel.Text = "Emp Name:"
        '
        'EmpNameComboBox
        '
        Me.EmpNameComboBox.FormattingEnabled = True
        Me.EmpNameComboBox.Location = New System.Drawing.Point(146, 112)
        Me.EmpNameComboBox.Name = "EmpNameComboBox"
        Me.EmpNameComboBox.Size = New System.Drawing.Size(121, 21)
        Me.EmpNameComboBox.TabIndex = 8
        '
        'TotalJobsLabel
        '
        TotalJobsLabel.AutoSize = True
        TotalJobsLabel.Location = New System.Drawing.Point(24, 142)
        TotalJobsLabel.Name = "TotalJobsLabel"
        TotalJobsLabel.Size = New System.Drawing.Size(59, 13)
        TotalJobsLabel.TabIndex = 9
        TotalJobsLabel.Text = "Total Jobs:"
        '
        'TotalJobsTextBox
        '
        Me.TotalJobsTextBox.Location = New System.Drawing.Point(146, 139)
        Me.TotalJobsTextBox.Name = "TotalJobsTextBox"
        Me.TotalJobsTextBox.Size = New System.Drawing.Size(121, 20)
        Me.TotalJobsTextBox.TabIndex = 10
        '
        'TotalSalaryLabel
        '
        TotalSalaryLabel.AutoSize = True
        TotalSalaryLabel.Location = New System.Drawing.Point(24, 168)
        TotalSalaryLabel.Name = "TotalSalaryLabel"
        TotalSalaryLabel.Size = New System.Drawing.Size(66, 13)
        TotalSalaryLabel.TabIndex = 11
        TotalSalaryLabel.Text = "Total Salary:"
        '
        'TotalSalaryTextBox
        '
        Me.TotalSalaryTextBox.Location = New System.Drawing.Point(146, 165)
        Me.TotalSalaryTextBox.Name = "TotalSalaryTextBox"
        Me.TotalSalaryTextBox.Size = New System.Drawing.Size(121, 20)
        Me.TotalSalaryTextBox.TabIndex = 12
        '
        'AllowancesLabel
        '
        AllowancesLabel.AutoSize = True
        AllowancesLabel.Location = New System.Drawing.Point(24, 194)
        AllowancesLabel.Name = "AllowancesLabel"
        AllowancesLabel.Size = New System.Drawing.Size(64, 13)
        AllowancesLabel.TabIndex = 13
        AllowancesLabel.Text = "Allowances:"
        '
        'AllowancesTextBox
        '
        Me.AllowancesTextBox.Location = New System.Drawing.Point(146, 191)
        Me.AllowancesTextBox.Name = "AllowancesTextBox"
        Me.AllowancesTextBox.Size = New System.Drawing.Size(121, 20)
        Me.AllowancesTextBox.TabIndex = 14
        '
        'MissingToolsChargesLabel
        '
        MissingToolsChargesLabel.AutoSize = True
        MissingToolsChargesLabel.Location = New System.Drawing.Point(24, 220)
        MissingToolsChargesLabel.Name = "MissingToolsChargesLabel"
        MissingToolsChargesLabel.Size = New System.Drawing.Size(116, 13)
        MissingToolsChargesLabel.TabIndex = 15
        MissingToolsChargesLabel.Text = "Missing Tools Charges:"
        '
        'MissingToolsChargesTextBox
        '
        Me.MissingToolsChargesTextBox.Location = New System.Drawing.Point(146, 217)
        Me.MissingToolsChargesTextBox.Name = "MissingToolsChargesTextBox"
        Me.MissingToolsChargesTextBox.Size = New System.Drawing.Size(121, 20)
        Me.MissingToolsChargesTextBox.TabIndex = 16
        '
        'DatePaidLabel
        '
        DatePaidLabel.AutoSize = True
        DatePaidLabel.Location = New System.Drawing.Point(24, 246)
        DatePaidLabel.Name = "DatePaidLabel"
        DatePaidLabel.Size = New System.Drawing.Size(57, 13)
        DatePaidLabel.TabIndex = 17
        DatePaidLabel.Text = "Date Paid:"
        '
        'DatePaidTextBox
        '
        Me.DatePaidTextBox.Location = New System.Drawing.Point(146, 243)
        Me.DatePaidTextBox.Name = "DatePaidTextBox"
        Me.DatePaidTextBox.Size = New System.Drawing.Size(121, 20)
        Me.DatePaidTextBox.TabIndex = 18
        '
        'frmPayroll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(580, 733)
        Me.Controls.Add(IDLabel)
        Me.Controls.Add(Me.IDTextBox)
        Me.Controls.Add(PayMonthLabel)
        Me.Controls.Add(Me.PayMonthTextBox)
        Me.Controls.Add(EmpIDLabel)
        Me.Controls.Add(Me.EmpIDTextBox)
        Me.Controls.Add(EmpNameLabel)
        Me.Controls.Add(Me.EmpNameComboBox)
        Me.Controls.Add(TotalJobsLabel)
        Me.Controls.Add(Me.TotalJobsTextBox)
        Me.Controls.Add(TotalSalaryLabel)
        Me.Controls.Add(Me.TotalSalaryTextBox)
        Me.Controls.Add(AllowancesLabel)
        Me.Controls.Add(Me.AllowancesTextBox)
        Me.Controls.Add(MissingToolsChargesLabel)
        Me.Controls.Add(Me.MissingToolsChargesTextBox)
        Me.Controls.Add(DatePaidLabel)
        Me.Controls.Add(Me.DatePaidTextBox)
        Me.Name = "frmPayroll"
        Me.Text = "frmPayroll"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents IDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PayMonthTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmpIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmpNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TotalJobsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TotalSalaryTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AllowancesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MissingToolsChargesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DatePaidTextBox As System.Windows.Forms.TextBox
End Class
