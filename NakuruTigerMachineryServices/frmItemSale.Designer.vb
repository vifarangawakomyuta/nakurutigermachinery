﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmItemSale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim IDLabel As System.Windows.Forms.Label
        Dim NameLabel As System.Windows.Forms.Label
        Dim SizeLabel As System.Windows.Forms.Label
        Dim QuantityLabel As System.Windows.Forms.Label
        Dim UnitPriceLabel As System.Windows.Forms.Label
        Dim ItemsSoldLabel As System.Windows.Forms.Label
        Dim TotalPriceLabel As System.Windows.Forms.Label
        Dim AmountGivenLabel As System.Windows.Forms.Label
        Dim BalanceLabel As System.Windows.Forms.Label
        Dim DatePaidLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmItemSale))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.ItemsSoldTextBox = New System.Windows.Forms.TextBox()
        Me.AmountGivenTextBox = New System.Windows.Forms.TextBox()
        Me.DatePaidDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.TotalPriceTextBox = New System.Windows.Forms.TextBox()
        Me.BalanceTextBox = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.NewItemButton = New System.Windows.Forms.Button()
        Me.QuantityItemTextBox = New System.Windows.Forms.TextBox()
        Me.btnAddToCart = New System.Windows.Forms.Button()
        Me.NameTextBox = New System.Windows.Forms.TextBox()
        Me.ItemIDComboBox = New System.Windows.Forms.ComboBox()
        Me.SizeTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        Me.UnitPriceTextBox = New System.Windows.Forms.TextBox()
        Me.PrintForm1 = New Microsoft.VisualBasic.PowerPacks.Printing.PrintForm(Me.components)
        IDLabel = New System.Windows.Forms.Label()
        NameLabel = New System.Windows.Forms.Label()
        SizeLabel = New System.Windows.Forms.Label()
        QuantityLabel = New System.Windows.Forms.Label()
        UnitPriceLabel = New System.Windows.Forms.Label()
        ItemsSoldLabel = New System.Windows.Forms.Label()
        TotalPriceLabel = New System.Windows.Forms.Label()
        AmountGivenLabel = New System.Windows.Forms.Label()
        BalanceLabel = New System.Windows.Forms.Label()
        DatePaidLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(14, 35)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(41, 13)
        IDLabel.TabIndex = 0
        IDLabel.Text = "ItemID:"
        '
        'NameLabel
        '
        NameLabel.AutoSize = True
        NameLabel.Location = New System.Drawing.Point(14, 61)
        NameLabel.Name = "NameLabel"
        NameLabel.Size = New System.Drawing.Size(38, 13)
        NameLabel.TabIndex = 2
        NameLabel.Text = "Name:"
        '
        'SizeLabel
        '
        SizeLabel.AutoSize = True
        SizeLabel.Location = New System.Drawing.Point(14, 88)
        SizeLabel.Name = "SizeLabel"
        SizeLabel.Size = New System.Drawing.Size(30, 13)
        SizeLabel.TabIndex = 4
        SizeLabel.Text = "Size:"
        '
        'QuantityLabel
        '
        QuantityLabel.AutoSize = True
        QuantityLabel.Location = New System.Drawing.Point(14, 114)
        QuantityLabel.Name = "QuantityLabel"
        QuantityLabel.Size = New System.Drawing.Size(49, 13)
        QuantityLabel.TabIndex = 6
        QuantityLabel.Text = "Quantity:"
        '
        'UnitPriceLabel
        '
        UnitPriceLabel.AutoSize = True
        UnitPriceLabel.Location = New System.Drawing.Point(14, 157)
        UnitPriceLabel.Name = "UnitPriceLabel"
        UnitPriceLabel.Size = New System.Drawing.Size(56, 13)
        UnitPriceLabel.TabIndex = 8
        UnitPriceLabel.Text = "Unit Price:"
        '
        'ItemsSoldLabel
        '
        ItemsSoldLabel.AutoSize = True
        ItemsSoldLabel.Location = New System.Drawing.Point(47, 29)
        ItemsSoldLabel.Name = "ItemsSoldLabel"
        ItemsSoldLabel.Size = New System.Drawing.Size(59, 13)
        ItemsSoldLabel.TabIndex = 12
        ItemsSoldLabel.Text = "Items Sold:"
        '
        'TotalPriceLabel
        '
        TotalPriceLabel.AutoSize = True
        TotalPriceLabel.Location = New System.Drawing.Point(47, 53)
        TotalPriceLabel.Name = "TotalPriceLabel"
        TotalPriceLabel.Size = New System.Drawing.Size(61, 13)
        TotalPriceLabel.TabIndex = 14
        TotalPriceLabel.Text = "Total Price:"
        '
        'AmountGivenLabel
        '
        AmountGivenLabel.AutoSize = True
        AmountGivenLabel.Location = New System.Drawing.Point(47, 79)
        AmountGivenLabel.Name = "AmountGivenLabel"
        AmountGivenLabel.Size = New System.Drawing.Size(77, 13)
        AmountGivenLabel.TabIndex = 16
        AmountGivenLabel.Text = "Amount Given:"
        '
        'BalanceLabel
        '
        BalanceLabel.AutoSize = True
        BalanceLabel.Location = New System.Drawing.Point(47, 105)
        BalanceLabel.Name = "BalanceLabel"
        BalanceLabel.Size = New System.Drawing.Size(49, 13)
        BalanceLabel.TabIndex = 18
        BalanceLabel.Text = "Balance:"
        '
        'DatePaidLabel
        '
        DatePaidLabel.AutoSize = True
        DatePaidLabel.Location = New System.Drawing.Point(47, 132)
        DatePaidLabel.Name = "DatePaidLabel"
        DatePaidLabel.Size = New System.Drawing.Size(57, 13)
        DatePaidLabel.TabIndex = 20
        DatePaidLabel.Text = "Date Paid:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(14, 138)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(102, 13)
        Label1.TabIndex = 6
        Label1.Text = "Remaining Quantity:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(894, 456)
        Me.Panel1.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(323, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(571, 456)
        Me.Panel3.TabIndex = 1
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.DataGridView1)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel5.Size = New System.Drawing.Size(571, 228)
        Me.Panel5.TabIndex = 2
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(10, 10)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(511, 208)
        Me.DataGridView1.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.btnSave)
        Me.Panel4.Controls.Add(ItemsSoldLabel)
        Me.Panel4.Controls.Add(TotalPriceLabel)
        Me.Panel4.Controls.Add(Me.ItemsSoldTextBox)
        Me.Panel4.Controls.Add(Me.AmountGivenTextBox)
        Me.Panel4.Controls.Add(AmountGivenLabel)
        Me.Panel4.Controls.Add(Me.DatePaidDateTimePicker)
        Me.Panel4.Controls.Add(BalanceLabel)
        Me.Panel4.Controls.Add(Me.TotalPriceTextBox)
        Me.Panel4.Controls.Add(DatePaidLabel)
        Me.Panel4.Controls.Add(Me.BalanceTextBox)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 228)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Padding = New System.Windows.Forms.Padding(10, 10, 50, 10)
        Me.Panel4.Size = New System.Drawing.Size(571, 228)
        Me.Panel4.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(356, 14)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(119, 32)
        Me.btnSave.TabIndex = 22
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'ItemsSoldTextBox
        '
        Me.ItemsSoldTextBox.Enabled = False
        Me.ItemsSoldTextBox.Location = New System.Drawing.Point(123, 26)
        Me.ItemsSoldTextBox.Name = "ItemsSoldTextBox"
        Me.ItemsSoldTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ItemsSoldTextBox.TabIndex = 13
        '
        'AmountGivenTextBox
        '
        Me.AmountGivenTextBox.Location = New System.Drawing.Point(123, 76)
        Me.AmountGivenTextBox.Name = "AmountGivenTextBox"
        Me.AmountGivenTextBox.Size = New System.Drawing.Size(200, 20)
        Me.AmountGivenTextBox.TabIndex = 17
        '
        'DatePaidDateTimePicker
        '
        Me.DatePaidDateTimePicker.CustomFormat = "dd-MMM-yy"
        Me.DatePaidDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DatePaidDateTimePicker.Location = New System.Drawing.Point(123, 128)
        Me.DatePaidDateTimePicker.Name = "DatePaidDateTimePicker"
        Me.DatePaidDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.DatePaidDateTimePicker.TabIndex = 21
        '
        'TotalPriceTextBox
        '
        Me.TotalPriceTextBox.Enabled = False
        Me.TotalPriceTextBox.Location = New System.Drawing.Point(123, 50)
        Me.TotalPriceTextBox.Name = "TotalPriceTextBox"
        Me.TotalPriceTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TotalPriceTextBox.TabIndex = 15
        '
        'BalanceTextBox
        '
        Me.BalanceTextBox.Enabled = False
        Me.BalanceTextBox.Location = New System.Drawing.Point(123, 102)
        Me.BalanceTextBox.Name = "BalanceTextBox"
        Me.BalanceTextBox.Size = New System.Drawing.Size(200, 20)
        Me.BalanceTextBox.TabIndex = 19
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.NewItemButton)
        Me.Panel2.Controls.Add(Me.QuantityItemTextBox)
        Me.Panel2.Controls.Add(Me.btnAddToCart)
        Me.Panel2.Controls.Add(IDLabel)
        Me.Panel2.Controls.Add(Me.NameTextBox)
        Me.Panel2.Controls.Add(NameLabel)
        Me.Panel2.Controls.Add(Me.ItemIDComboBox)
        Me.Panel2.Controls.Add(SizeLabel)
        Me.Panel2.Controls.Add(Me.SizeTextBox)
        Me.Panel2.Controls.Add(Label1)
        Me.Panel2.Controls.Add(QuantityLabel)
        Me.Panel2.Controls.Add(Me.QuantityTextBox)
        Me.Panel2.Controls.Add(UnitPriceLabel)
        Me.Panel2.Controls.Add(Me.UnitPriceTextBox)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(100, 10, 10, 10)
        Me.Panel2.Size = New System.Drawing.Size(327, 456)
        Me.Panel2.TabIndex = 0
        '
        'NewItemButton
        '
        Me.NewItemButton.Location = New System.Drawing.Point(17, 214)
        Me.NewItemButton.Name = "NewItemButton"
        Me.NewItemButton.Size = New System.Drawing.Size(137, 45)
        Me.NewItemButton.TabIndex = 12
        Me.NewItemButton.Text = "New Sale"
        Me.NewItemButton.UseVisualStyleBackColor = True
        '
        'QuantityItemTextBox
        '
        Me.QuantityItemTextBox.Enabled = False
        Me.QuantityItemTextBox.Location = New System.Drawing.Point(114, 135)
        Me.QuantityItemTextBox.Name = "QuantityItemTextBox"
        Me.QuantityItemTextBox.Size = New System.Drawing.Size(200, 20)
        Me.QuantityItemTextBox.TabIndex = 11
        '
        'btnAddToCart
        '
        Me.btnAddToCart.Location = New System.Drawing.Point(179, 214)
        Me.btnAddToCart.Name = "btnAddToCart"
        Me.btnAddToCart.Size = New System.Drawing.Size(135, 45)
        Me.btnAddToCart.TabIndex = 10
        Me.btnAddToCart.Text = "AddToCart"
        Me.btnAddToCart.UseVisualStyleBackColor = True
        '
        'NameTextBox
        '
        Me.NameTextBox.Enabled = False
        Me.NameTextBox.Location = New System.Drawing.Point(114, 59)
        Me.NameTextBox.Name = "NameTextBox"
        Me.NameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.NameTextBox.TabIndex = 1
        '
        'ItemIDComboBox
        '
        Me.ItemIDComboBox.FormattingEnabled = True
        Me.ItemIDComboBox.Location = New System.Drawing.Point(114, 32)
        Me.ItemIDComboBox.Name = "ItemIDComboBox"
        Me.ItemIDComboBox.Size = New System.Drawing.Size(200, 21)
        Me.ItemIDComboBox.TabIndex = 3
        Me.ItemIDComboBox.Text = "--Select Item ID--"
        '
        'SizeTextBox
        '
        Me.SizeTextBox.Enabled = False
        Me.SizeTextBox.Location = New System.Drawing.Point(114, 85)
        Me.SizeTextBox.Name = "SizeTextBox"
        Me.SizeTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SizeTextBox.TabIndex = 5
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.Location = New System.Drawing.Point(114, 109)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(200, 20)
        Me.QuantityTextBox.TabIndex = 7
        Me.QuantityTextBox.Text = "0"
        '
        'UnitPriceTextBox
        '
        Me.UnitPriceTextBox.Enabled = False
        Me.UnitPriceTextBox.Location = New System.Drawing.Point(114, 161)
        Me.UnitPriceTextBox.Name = "UnitPriceTextBox"
        Me.UnitPriceTextBox.Size = New System.Drawing.Size(200, 20)
        Me.UnitPriceTextBox.TabIndex = 9
        '
        'PrintForm1
        '
        Me.PrintForm1.DocumentName = "document"
        Me.PrintForm1.Form = Me
        Me.PrintForm1.PrintAction = System.Drawing.Printing.PrintAction.PrintToPrinter
        Me.PrintForm1.PrinterSettings = CType(resources.GetObject("PrintForm1.PrinterSettings"), System.Drawing.Printing.PrinterSettings)
        Me.PrintForm1.PrintFileName = Nothing
        '
        'frmItemSale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 456)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmItemSale"
        Me.Text = "frmItemSale"
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents NameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemIDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents SizeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents QuantityTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UnitPriceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemsSoldTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TotalPriceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AmountGivenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BalanceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DatePaidDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents btnAddToCart As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents QuantityItemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NewItemButton As System.Windows.Forms.Button
    Friend WithEvents PrintForm1 As Microsoft.VisualBasic.PowerPacks.Printing.PrintForm
End Class
