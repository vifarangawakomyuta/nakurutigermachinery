﻿Imports MySql.Data.MySqlClient
Public Class frmUsers

    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader

    Private Sub loadtable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.users"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub loadUserTypetable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.usertype"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView2.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub searchTable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.users Where UserName like '%" + txtSearch.Text + "%'"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub searchUserTypeTable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.usertype Where TypeName like '%" + txtsearch2.Text + "%'"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView2.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub loadusertype()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.usertype"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read
                Dim sname = reader.GetString("TypeName")
                UserTypeTextBox.Items.Add(sname)


            End While
            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
        'Call formAddItems()
    End Sub
    Private Sub populateUserTypes()
        Try
            Call openconn()
            Dim query As String
            query = "select * from ntmsdata.usertype where TypeName = '" & UserTypeTextBox.Text & "'"
            cmd = New MySqlCommand(query, con)
            reader = cmd.ExecuteReader
            While reader.Read

                UserTypeIDTextBox.Text = reader.GetString("ID")


            End While
            Call closeconn()
            loadtable()



        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub

    Private Sub clear()
        IDTextBox.Clear()
        UserTypeIDTextBox.Clear()
        UserTypeTextBox.Text = "--select usertype--"
        UserNameTextBox.Clear()
        PasswordTextBox.Clear()
    End Sub
    Private Sub clearUserType()
        IDTextBox1.Clear()
        TypeNameTextBox.Clear()
       
    End Sub

    Private Sub saveUsers()
        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.users ( UserTypeID,UserType , UserName, Password) Values (  '" & UserTypeIDTextBox.Text & "',  '" & UserTypeTextBox.Text & "',  '" & UserNameTextBox.Text & "',     '" & PasswordTextBox.Text & "'  )"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If UserTypeTextBox.Text = "--select usertype--" Or UserTypeTextBox.Text = "" Then
                MsgBox("Please select user type", MsgBoxStyle.Critical, "warning")
                UserTypeTextBox.Focus()
            ElseIf UserNameTextBox.Text = "" Then
                MsgBox("Please Enter User name", MsgBoxStyle.Critical, "warning")
                UserNameTextBox.Focus()
            ElseIf PasswordTextBox.Text = "" Then
                MsgBox("Please Enter password", MsgBoxStyle.Critical, "warning")
                PasswordTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()
                MsgBox("Save Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub saveUserType()
        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.usertype (TypeName) Values (  '" & TypeNameTextBox.Text & "'  )"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If TypeNameTextBox.Text = "" Then
                MsgBox("Please Enter type Name", MsgBoxStyle.Critical, "warning")
                TypeNameTextBox.Focus()
            
            Else
                cmd.ExecuteNonQuery()
                MsgBox("Save Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadUserTypetable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub updateUsers()
        Try

            Call openconn()
            Dim query As String
            query = "Update ntmsdata.users set  UserTypeID='" & UserTypeIDTextBox.Text & "', UserType = '" & UserTypeTextBox.Text & "',   UserName ='" & UserNameTextBox.Text & "',     Password ='" & PasswordTextBox.Text & "' where ID ='" & IDTextBox.Text & "'"
            cmd = New MySqlCommand(query, con)

            cmd.Connection = con

            If UserTypeTextBox.Text = "--select usertype--" Or UserTypeTextBox.Text = "" Then
                MsgBox("Please select user type", MsgBoxStyle.Critical, "warning")
                UserTypeTextBox.Focus()
            ElseIf UserNameTextBox.Text = "" Then
                MsgBox("Please Enter User name", MsgBoxStyle.Critical, "warning")
                UserNameTextBox.Focus()
            ElseIf PasswordTextBox.Text = "" Then
                MsgBox("Please Enter password", MsgBoxStyle.Critical, "warning")
                PasswordTextBox.Focus()
            Else
                cmd.ExecuteNonQuery()

                MsgBox("Update Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub updateUserTypes()
        Try

            Call openconn()
            Dim query As String
            query = "Update ntmsdata.usertype set  TypeName='" & TypeNameTextBox.Text & "' where ID ='" & IDTextBox1.Text & "'"
            cmd = New MySqlCommand(query, con)

            cmd.Connection = con

            If TypeNameTextBox.Text = "" Then
                MsgBox("Please Enter type Name", MsgBoxStyle.Critical, "warning")
                TypeNameTextBox.Focus()

            Else
                cmd.ExecuteNonQuery()

                MsgBox("Update Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadUserTypetable()
                loadusertype()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub deleteUsers()
        Try
            If MessageBox.Show("Do you really want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then

                Call openconn()
                Dim query As String
                query = "delete from ntmsdata.users where ID ='" & IDTextBox.Text & "'  "
                cmd = New MySqlCommand(query, con)
                reader = cmd.ExecuteReader
                MsgBox("Delete Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub deleteUserTypes()
        Try
            If MessageBox.Show("Do you really want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then

                Call openconn()
                Dim query As String
                query = "delete from ntmsdata.usertype where ID ='" & IDTextBox1.Text & "'  "
                cmd = New MySqlCommand(query, con)
                reader = cmd.ExecuteReader
                MsgBox("Delete Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub

   
   
    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If e.RowIndex >= 0 Then
            'gets a collection that contains all the rows
            Dim row As DataGridViewRow = Me.DataGridView1.Rows(e.RowIndex)
            'populate the textbox from specific value of the coordinates of column and row.


            IDTextBox.Text = row.Cells(0).Value.ToString()
            UserTypeIDTextBox.Text = row.Cells(1).Value.ToString()
            UserTypeTextBox.Text = row.Cells(2).Value.ToString()
            UserNameTextBox.Text = row.Cells(3).Value.ToString()
            PasswordTextBox.Text = row.Cells(4).Value.ToString()


            If IDTextBox.Text = "" Then
                MsgBox("Please Select A row with data on the datagrid View below", MsgBoxStyle.Critical)

            End If

        End If
    End Sub

    Private Sub DataGridView2_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        If e.RowIndex >= 0 Then
            'gets a collection that contains all the rows
            Dim row As DataGridViewRow = Me.DataGridView2.Rows(e.RowIndex)
            'populate the textbox from specific value of the coordinates of column and row.


            IDTextBox1.Text = row.Cells(0).Value.ToString()
            TypeNameTextBox.Text = row.Cells(1).Value.ToString()
            


            If IDTextBox1.Text = "" Then
                MsgBox("Please Select A row with data on the datagrid View below", MsgBoxStyle.Critical)

            End If

        End If
    End Sub

    Private Sub UserTypeTextBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles UserTypeTextBox.SelectedIndexChanged



    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        clear()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        saveUsers()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        updateUsers()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        deleteUsers()
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        loadtable()
        searchTable()
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        clearUserType()
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        saveUserType()
        UserTypeTextBox.Items.Clear()
        loadusertype()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        updateUserTypes()
        UserTypeTextBox.Items.Clear()
        loadusertype()

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        deleteUserTypes()
        UserTypeTextBox.Items.Clear()
        loadusertype()
    End Sub

    Private Sub frmUsers_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadtable()
        loadUserTypetable()
        loadusertype()
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub UserTypeTextBox_TextChanged(sender As Object, e As EventArgs) Handles UserTypeTextBox.TextChanged
        populateUserTypes()
    End Sub

    Private Sub txtsearch2_TextChanged(sender As Object, e As EventArgs) Handles txtsearch2.TextChanged
        loadUserTypetable()
        searchUserTypeTable()
    End Sub

    Private Sub Panel3_Paint(sender As Object, e As PaintEventArgs) Handles Panel3.Paint

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class