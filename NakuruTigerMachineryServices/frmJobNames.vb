﻿Imports MySql.Data.MySqlClient
Public Class frmJobNames

    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader

    Private Sub loadtable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.JobNames"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub
    Private Sub searchTable()
        Try
            Call openconn()
            Dim query As String
            Dim ada As New MySqlDataAdapter
            Dim dalpset As New DataTable
            Dim dalpsource As New BindingSource
            query = "select * from ntmsdata.JobNames Where Name like '%" + txtSearch.Text + "%'"
            cmd = New MySqlCommand(query, con)
            ada.SelectCommand = cmd
            ada.Fill(dalpset)
            dalpsource.DataSource = dalpset
            DataGridView1.DataSource = dalpsource
            ada.Update(dalpset)

            Call closeconn()

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
            con.Dispose()

        End Try
    End Sub

    Private Sub clearJobNames()
        IDTextBox.Clear()
        NameTextBox.Clear()


    End Sub
    Private Sub saveJobNames()
        Try
            Call openconn()
            Dim query As String
            query = "insert into ntmsdata.JobNames ( Name, Description) Values (  '" & NameTextBox.Text & "' ,'" & DescriptionTextBox.Text & "' )"
            cmd = New MySqlCommand(query, con)
            cmd.Connection = con

            If NameTextBox.Text = "" Then
                MsgBox("Please Enter Job or Item Name", MsgBoxStyle.Critical, "warning")
                NameTextBox.Focus()
            ElseIf DescriptionTextBox.Text = "" Then
                MsgBox("Please Enter Job Description or size of Item", MsgBoxStyle.Critical, "warning")
                DescriptionTextBox.Focus()

            Else
                cmd.ExecuteNonQuery()
                MsgBox("Save Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub updateJobNames()
        Try

            Call openconn()
            Dim query As String
            query = "Update ntmsdata.JobNames set  Name='" & NameTextBox.Text & "', Description='" & DescriptionTextBox.Text & "'  where IDJobNames ='" & IDTextBox.Text & "'"
            cmd = New MySqlCommand(query, con)

            cmd.Connection = con

            If NameTextBox.Text = "" Then
                MsgBox("Please Enter Job or Item Name", MsgBoxStyle.Critical, "warning")
                NameTextBox.Focus()
            ElseIf DescriptionTextBox.Text = "" Then
                MsgBox("Please Enter Job Description or size of Item", MsgBoxStyle.Critical, "warning")
                DescriptionTextBox.Focus()

            Else
                cmd.ExecuteNonQuery()

                MsgBox("Update Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub
    Private Sub deleteJobNames()
        Try
            If MessageBox.Show("Do you really want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then

                Call openconn()
                Dim query As String
                query = "delete from ntmsdata.JobNames where IDJobNames ='" & IDTextBox.Text & "'  "
                cmd = New MySqlCommand(query, con)
                reader = cmd.ExecuteReader
                MsgBox("Delete Sucessful", MsgBoxStyle.Information)
                Call closeconn()
                loadtable()
            End If
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)

            con.Dispose()
        End Try
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        clearJobNames()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Call saveJobNames()

    End Sub


    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Call deleteJobNames()
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If e.RowIndex >= 0 Then
            'gets a collection that contains all the rows
            Dim row As DataGridViewRow = Me.DataGridView1.Rows(e.RowIndex)
            'populate the textbox from specific value of the coordinates of column and row.


            IDTextBox.Text = row.Cells(0).Value.ToString()
            NameTextBox.Text = row.Cells(1).Value.ToString()
            DescriptionTextBox.Text = row.Cells(2).Value.ToString()
            
            If IDTextBox.Text = "" Then
                MsgBox("Please Select A row with data on the datagrid View below", MsgBoxStyle.Critical)

            End If

        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Call loadtable()
        Call searchTable()
    End Sub

    Private Sub frmJobNames_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call loadtable()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        updateJobNames()
    End Sub
End Class