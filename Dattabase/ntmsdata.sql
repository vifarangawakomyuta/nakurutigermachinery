SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `ntmsdata` DEFAULT CHARACTER SET utf8 ;
USE `ntmsdata` ;

-- -----------------------------------------------------
-- Table `ntmsdata`.`customers`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`customers` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `CustName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Company` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Telephone` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`items`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`items` (
  `ID` INT(255) NOT NULL ,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Size` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT '0' ,
  `UnitPrice` INT(11) NULL DEFAULT '0' ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`depositsaleitemlist`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`depositsaleitemlist` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ItemID` INT(11) NULL DEFAULT NULL ,
  `ItemName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Size` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Remaining` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Price` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `CustID` INT(11) NULL DEFAULT NULL ,
  `CustName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `CustID` (`CustID` ASC) ,
  INDEX `ItemID` (`ItemID` ASC) ,
  CONSTRAINT `fk_depositsaleitemlist_customers1`
    FOREIGN KEY (`CustID` )
    REFERENCES `ntmsdata`.`customers` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_depositsaleitemlist_items1`
    FOREIGN KEY (`ItemID` )
    REFERENCES `ntmsdata`.`items` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`depositsales`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`depositsales` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ItemsSold` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `TotalPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `AmountDeposited` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateDeposited` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `AmountDue` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DueDate` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Fine` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `CustID` INT(11) NULL DEFAULT NULL ,
  `CustName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `CustomerID` (`Fine` ASC) ,
  INDEX `fk_depositsales_customers1` (`CustID` ASC) ,
  CONSTRAINT `fk_depositsales_customers1`
    FOREIGN KEY (`CustID` )
    REFERENCES `ntmsdata`.`customers` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`employee`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`employee` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `FirstName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `LastName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Telephone` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`suppliers`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`suppliers` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `SupName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Company` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Telephone` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EMail` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`invoice`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`invoice` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `SupID` INT(11) NULL DEFAULT NULL ,
  `SupName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Company` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `InvoiceDate` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Total` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `AmountGiven` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Balance` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `SupID` (`SupID` ASC) ,
  CONSTRAINT `fk_invoice_suppliers1`
    FOREIGN KEY (`SupID` )
    REFERENCES `ntmsdata`.`suppliers` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`itemcomponents`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`itemcomponents` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Size` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UnitPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`itemcomponentsale`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`itemcomponentsale` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ItemComponentsSold` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `TotalPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `AmountGiven` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Balance` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateSold` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`itemcomponentssalelist`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`itemcomponentssalelist` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ItemComponentID` INT(11) NULL DEFAULT NULL ,
  `ItemComponentName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Size` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UnitPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Remaining` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DatePaid` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `DateSPaid` (`DatePaid` ASC) ,
  INDEX `ItemComponentID` (`ItemComponentID` ASC) ,
  CONSTRAINT `fk_itemcomponentssalelist_itemcomponents1`
    FOREIGN KEY (`ItemComponentID` )
    REFERENCES `ntmsdata`.`itemcomponents` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 57
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`jobnames`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`jobnames` (
  `idJobNames` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(45) NULL DEFAULT NULL ,
  `Description` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`idJobNames`) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ntmsdata`.`jobs`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`jobs` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `JobNameID` INT(11) NULL DEFAULT NULL ,
  `JobName` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `JobDescription` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EmpID` INT(11) NULL DEFAULT NULL ,
  `EmpName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `StartDate` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EndDate` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Wage` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `EmpID` (`EmpID` ASC) ,
  INDEX `fk_jobs_JobNames1` (`JobNameID` ASC) ,
  CONSTRAINT `fk_jobs_JobNames1`
    FOREIGN KEY (`JobNameID` )
    REFERENCES `ntmsdata`.`jobnames` (`idJobNames` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_jobs_employee1`
    FOREIGN KEY (`EmpID` )
    REFERENCES `ntmsdata`.`employee` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 20
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`usertype`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`usertype` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `TypeName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`users` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `UserTypeID` INT(11) NULL DEFAULT NULL ,
  `UserType` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UserName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Password` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `UserTypeID` (`UserTypeID` ASC) ,
  CONSTRAINT `fk_users_usertype`
    FOREIGN KEY (`UserTypeID` )
    REFERENCES `ntmsdata`.`usertype` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`logs`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`logs` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ActivityName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Time` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UserID` INT(11) NULL DEFAULT NULL ,
  `UserName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `UserID` (`UserID` ASC) ,
  CONSTRAINT `fk_logs_users1`
    FOREIGN KEY (`UserID` )
    REFERENCES `ntmsdata`.`users` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`missingtools`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`missingtools` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `MissingTools` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EmpID` INT(11) NULL DEFAULT NULL ,
  `EmpName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateLost` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `TotalValue` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `EmpID` (`EmpID` ASC) ,
  CONSTRAINT `fk_missingtools_employee1`
    FOREIGN KEY (`EmpID` )
    REFERENCES `ntmsdata`.`employee` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`tools`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`tools` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ToolName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Size` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UnitValue` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `ToolValue` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`missingtoolslist`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`missingtoolslist` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ToolID` INT(11) NULL DEFAULT NULL ,
  `ToolName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `ToolValue` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EmpID` INT(11) NULL DEFAULT NULL ,
  `EmpName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateLost` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `EmpID` (`EmpID` ASC) ,
  INDEX `fk_missingtoolslist_tools1` (`ToolID` ASC) ,
  CONSTRAINT `fk_missingtoolslist_employee1`
    FOREIGN KEY (`EmpID` )
    REFERENCES `ntmsdata`.`employee` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_missingtoolslist_tools1`
    FOREIGN KEY (`ToolID` )
    REFERENCES `ntmsdata`.`tools` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`oneoffsales`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`oneoffsales` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ItemsSold` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `TotalPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `AmountGiven` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Balance` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DatePaid` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 54
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`payroll`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`payroll` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `PayMonth` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `EmpID` INT(11) NULL DEFAULT NULL ,
  `EmpName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `TotalJobs` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `TotalSalary` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `Allowances` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `MissingToolsCharges` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `DatePaid` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `DatePaid` (`DatePaid` ASC) ,
  INDEX `EmpID` (`EmpID` ASC) ,
  CONSTRAINT `fk_payroll_employee1`
    FOREIGN KEY (`EmpID` )
    REFERENCES `ntmsdata`.`employee` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `ntmsdata`.`profile`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`profile` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `CompanyName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Address` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EMail1` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EMail2` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EMail3` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Telephone1` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Telephone2` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Telephone3` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Website` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Facebook` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Twitter` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Logo` LONGBLOB NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`quotation`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`quotation` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ItemsQuoted` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `TotalPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DeliverlyType` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `PaymentMode` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `TransportType` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `QuoteExpiryDate` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `CustomerID` INT(11) NULL DEFAULT NULL ,
  `CustomerName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Company` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `CustomerID` (`CustomerID` ASC) ,
  CONSTRAINT `fk_quotation_customers1`
    FOREIGN KEY (`CustomerID` )
    REFERENCES `ntmsdata`.`customers` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`quotationlist`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`quotationlist` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ItemID` INT(11) NULL DEFAULT NULL ,
  `ItemName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Size` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UnitPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `ItemID` (`ItemID` ASC) ,
  CONSTRAINT `fk_quotationlist_items1`
    FOREIGN KEY (`ItemID` )
    REFERENCES `ntmsdata`.`items` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`solditemslist`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`solditemslist` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ItemID` INT(11) NULL DEFAULT NULL ,
  `ItemName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Size` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Remaining` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UnitPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateSold` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `ItemID` (`ItemID` ASC) ,
  CONSTRAINT `fk_solditemslist_items1`
    FOREIGN KEY (`ItemID` )
    REFERENCES `ntmsdata`.`items` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 130
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`supplies`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`supplies` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ItemID` INT(11) NULL DEFAULT NULL ,
  `ItemName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UnitPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `TotalPrice` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `SupID` INT(11) NULL DEFAULT NULL ,
  `SupName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Company` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateSupplied` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `InvoiceID` INT(11) NULL DEFAULT NULL ,
  `InvoiceNo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `AmountGiven` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Balance` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `InvoiceID` (`InvoiceID` ASC) ,
  INDEX `ItemID` (`ItemID` ASC) ,
  INDEX `fk_supplies_suppliers1` (`SupID` ASC) ,
  CONSTRAINT `fk_supplies_invoice1`
    FOREIGN KEY (`InvoiceID` )
    REFERENCES `ntmsdata`.`invoice` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_supplies_items1`
    FOREIGN KEY (`ItemID` )
    REFERENCES `ntmsdata`.`items` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_supplies_items2`
    FOREIGN KEY (`ItemID` )
    REFERENCES `ntmsdata`.`items` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_supplies_suppliers1`
    FOREIGN KEY (`SupID` )
    REFERENCES `ntmsdata`.`suppliers` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_supplies_suppliers2`
    FOREIGN KEY (`SupID` )
    REFERENCES `ntmsdata`.`suppliers` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`toolborrowing`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`toolborrowing` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `TotalTools` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateBorrowed` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EmpID` INT(11) NULL DEFAULT NULL ,
  `EmpName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `fk_toolborrowing_employee1` (`EmpID` ASC) ,
  CONSTRAINT `fk_toolborrowing_employee1`
    FOREIGN KEY (`EmpID` )
    REFERENCES `ntmsdata`.`employee` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`toolsborrowedlist`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`toolsborrowedlist` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ToolID` INT(11) NULL DEFAULT NULL ,
  `ToolName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UnitValue` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `ToolValue` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EmpID` INT(11) NULL DEFAULT NULL ,
  `EmpName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateBorrowed` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `EmpID` (`EmpID` ASC) ,
  INDEX `ToolID` (`ToolID` ASC) ,
  CONSTRAINT `fk_toolsborrowedlist_employee1`
    FOREIGN KEY (`EmpID` )
    REFERENCES `ntmsdata`.`employee` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_toolsborrowedlist_tools1`
    FOREIGN KEY (`ToolID` )
    REFERENCES `ntmsdata`.`tools` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`toolsreturned`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`toolsreturned` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `BorrowID` INT(11) NULL DEFAULT NULL ,
  `TotalTools` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateBorrowed` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateReturned` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EmpID` INT(11) NULL DEFAULT NULL ,
  `EmpName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `ToolsReturned` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `MissingTools` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `BorrowID` (`BorrowID` ASC) ,
  INDEX `EmpID` (`EmpID` ASC) ,
  CONSTRAINT `fk_toolsreturned_employee1`
    FOREIGN KEY (`EmpID` )
    REFERENCES `ntmsdata`.`employee` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_toolsreturned_toolborrowing1`
    FOREIGN KEY (`BorrowID` )
    REFERENCES `ntmsdata`.`toolborrowing` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ntmsdata`.`toolsreturnedlist`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ntmsdata`.`toolsreturnedlist` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ToolID` INT(11) NULL DEFAULT NULL ,
  `ToolName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `UnitValue` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `ToolValue` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Quantity` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `Remaining` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `EmpID` INT(11) NULL DEFAULT NULL ,
  `EmpName` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DateReturned` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `EmpID` (`EmpID` ASC) ,
  INDEX `ToolID` (`ToolID` ASC) ,
  CONSTRAINT `fk_toolsreturnedlist_employee1`
    FOREIGN KEY (`EmpID` )
    REFERENCES `ntmsdata`.`employee` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_toolsreturnedlist_tools1`
    FOREIGN KEY (`ToolID` )
    REFERENCES `ntmsdata`.`tools` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
